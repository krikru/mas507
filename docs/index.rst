.. MAS507 documentation master file, created by
   sphinx-quickstart on Tue Aug 25 21:04:02 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. image:: /figs/uia.png
   :width: 600px
   :align: center
   :alt: alternate text

|

.. raw:: html

   <h2 style="text-align: center">University of Agder</h2>
   <p style="text-align: center">Department of Engineering Sciences<br>Grimstad, Norway</p>
   
|

MAS-507 Product Development and Project Management.
==========================================================

This report covers the development procedure for the Barely Autonomous Robot To Optimize Strawberry Harvesting, BARTOSH.

Abstract
------------------------
An indoor localization system is developed, implemented as an extended Kalamn filter using AruCo markers as absolute land marks.
Further a strawberry harvesting and storage device is developed for the specified task. The robot body is also further developed to encapsulate and make the robot more human friendly.
A scalable control algorithm is developed to drive the robot from one bush to the next autonomously.
All the code developed is designed with a vision for a high degree of reusability and modularity.

The system is lastly tested and is able to keep track of its location, even when no land mark is in sight. Navigate from bush to bush, and detect whether a berry is ready for harvesting. The harvester is also able to carefully release the berries from the bushes and place them in the storage hopper.


.. figure:: /figs/product-development/BerryPicker.jpg
    :scale: 40%
    :align: center

    The final design of the robot, placed on the simulated strawberry-field



It is highly encouraged to read the report along side the code found in the gitlab repo, the report mainly covers the coding concepts and design decisions, where as the repo gives insight into the implementation of the concepts.


Video
-----------------------------
`Youtube video of the robot. <https://www.youtube.com/watch?v=6mxflmKS8BI>`_ 

Git repo
-----------------------------
`Link to Git repo <https://gitlab.com/krikru/mas507>`_ 


Group members:
------------------------

**Group number:** 5

| **Marius Egeland**
| Mechatronics fjomp
| marius-egeland@hotmail.com
| `Linkedin <https://www.linkedin.com/in/marius-egeland/>`_ 

| **Kristoffer Hansen Kruithof**
| Former child
| kristoffer.kru@gmail.com
| `Linkedin <https://www.linkedin.com/in/kristoffer-hansen-kruithof/>`_ 

| **Harald Sangvik**
| Solidworks wrestler
| harald.sangvik@gmail.com
| `Linkedin <https://www.linkedin.com/in/harald-sangvik/>`_ 

Lecturers
------------------------

| **Sondre Sanden Tørdal, PhD**
| Postdoctoral Fellow in Mechatronics
| https://www.uia.no/kk/profil/sondrest

| **Mette Mo Jakobsen, PhD**
| Professor in Product Development and Project-based learning
| https://www.uia.no/kk/profil/mettemj


Table of contents
===================================================

.. toctree::
   :maxdepth: 2
   :caption: BARTOSH:

   src/Introduction.rst
   src/theory
   src/requierments
   src/concepts
   src/methods
   src/results
   src/DiscussionConclusion.rst
   src/references
   src/appendix


.. src/start
   src/report
   src/ros
   src/ROS-setup
   src/Positioning

.. Indices and tables
.. ==================

.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
