Theory
======================

.. include:: theory/ROS.rst

.. include:: theory/StrawberryDetection.rst

.. include:: theory/kalman.rst

.. include:: theory/HomogeniousTransformation.rst

.. include:: theory/IMU.rst

.. include:: theory/DifferentialDriveOdometry.rst

.. include:: theory/productDevel.rst

.. include:: theory/StateMachine.rst

