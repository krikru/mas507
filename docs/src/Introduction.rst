Introduction
===============================

This report covers the compulsory exam report for MAS-507 Product Development and Project Management.

The task for the project is to develop, prototype, project manage and implement a robot for autonomous strawberry harvesting.
The platform for the robot is a small 3d printed frame that is driven using a differential drive locomotion. The computational unit on the robot is a nvidia Jetson Nano development board.

Using this platform as a starting point we are tasked with further developing the platform into a reliable prototype for strawberry harvesting.

The strawberry field for this robot is a somewhat artificial setup, but the core functionality can be demonstrated on the field.

**Namely:**

* Autonomy
* Localization of the robot
* Localization of strawberries
* Harvester design and functionality
* Repeatability

.. figure:: /figs/introduction/BerryFiled.png
    :align: center

    Strawberry field with ArUco markers

The majority of the software deployed on the robot is written with an object oriented and highly modular design philosophy for a high degree of reusability for similar projects and products. This in combination with a trackable development cycle with the use of git makes for simple expandability of the project.

The documentation of the code is focused on the design of the software and how the different elements interact and exchange data. For the specifics surrounding the code itself it is highly encouraged to read the code on the gitlab repository alongside the report.

**Link to gitlab repository:** `https://gitlab.com/krikru/mas507 <https://gitlab.com/krikru/mas507>`_  [Repo]_

The development of the strawberry harvester was done by first evaluating different concepts, once a concept was decided upon, it was further developed and improved after almost every test of the system. Rapid manufacturing and prototyping techniques allows for this rapid evolution of the product.



