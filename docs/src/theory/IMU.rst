Inertial Measurement Unit
-----------------------------

An Inertial Measurement Unit (IMU for short) is a device for measuring and reporting acceleration and angular rate of a body. An IMU often consists of three perpendicular 
accelerometers providing the acceleration in the X-,Y- and Z-direction in the units local coordinate-system and three gyroscopes giving the angular rate around each of these axes.
These rates are often referred to as roll, pitch and yaw respectively. [IMU]_

.. figure:: /figs/Theory/imu.png
    :align: center
    :width: 400px

    Example of an IMU with axes drawn