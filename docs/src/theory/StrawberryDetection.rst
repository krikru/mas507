.. _Image_segmentation:

Image segmentation 
--------------------

As an image often contains multiple megapixels worth of data where each pixel contains three colour channels, the amount of data contained within one image is quite substantial.
Image segmentation is a process where the pixels in an image is partitioned into sets known as "objects", where the goal is to make the image easier to analyze by removing the
unwanted parts of the image. [ImSeg]_

Colour segmentation
~~~~~~~~~~~~~~~~~~~~~~

Colour segmentation is the process of masking out all the pixels of a wanted colour in order to process only the parts of an image containing pixels of some specific colour or in 
some specific colour range. [ImSeg]_


Colour representation
~~~~~~~~~~~~~~~~~~~~~~

There are multiple different ways to represent colour, one way is to use the "Hue, Saturation, Value" or "HSV" representation. This can be thought of as dividing the colour
spectrum into a cylinder and describing the colour as a specific angle, radius and height combination as seen in the below figure. [HSV]_

.. figure:: /figs/Theory/HSV_color_solid_cylinder.png
    :width: 400px
    :align: center
    :name: HSVCyl
    
    HSV colour cylinder
   

