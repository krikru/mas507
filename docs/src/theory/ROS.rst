.. _ROS:

Robot Operating system
-------------------------------

"Robot Operating System" or "ROS" for short is a framework for writing robot software. It is a set of tools that aims to make creating modular
robot software easier by allowing programs (nodes) to communicate across multiple machines or internally using pre-defined topics. The ROS ecosystem includes a lot of 
pre-compiled packages and tools to boost development of high complexity robot software. The main programming languages for ROS are C++ and Python. [ROS]_

The ROS network is governed by a master, which registers all the nodes in the network and what topics they advertise and subscribe to. The ROS master connects the advertising 
nodes to the subscribing ones at node startup and the communication is direct (publisher -> subscriber) from then. The ROS communication-principle can be seen in the below figure.

.. figure:: /figs/Theory/ROS_System_Model.jpg
    :width: 600px
    :align: center
    :name: ROS_System_Model

    An overview of a simple ROS-network

ROS lays the foundation for distributed computing by allowing nodes on multiple machines on the ROS-network to communicate with each other over for example WiFi.


