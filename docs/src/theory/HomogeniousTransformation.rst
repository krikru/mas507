Homogeneous transformations
-----------------------------

A homogeneous transformation is a transformation that both translates and rotates a coordinate frame from one point in space to another.
The great value in using homogeneous transformations is that both the rotation and translation is done with one matrix operation.
This requires that a transformation in 3 dimensional space is done with a 4 dimensional matrix.

The matrix is on the form:

.. math::
    H^F_T = \begin{bmatrix}
    R & T \\ 
    0 & 1
    \end{bmatrix}

Where :math:`R` is a rotation matrix in the special orthogonal set and :math:`T` the translation between the center of one coordinate frame to the center of the other coordinate frame.

The subscript and superscript on the H matrix is read F = from frame, T = to frame.
