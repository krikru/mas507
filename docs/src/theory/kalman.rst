Kalman filter
----------------

A kalman filter is an advanced sensor fusion algorithm for estimating the state of a system.
The state of the system varies from application to application, but in the general case, it is the measures needed to fully describe the system state.

The estimated states of the system are reconstructed from an analytical state space model and measurements that are typically noisy and not alone able to measure the full state of the system.


The filter works in two main steps:

1. Prediction
2. Correction

In the first step, a prediction is made of the evolution of the state of the system into the next time step.
This estimation is based on the previously estimated and corrected state of the system, as well as the inputs given to the system.
Therefore the filter is said to be a recursive filter.
Along with the predicted state, the state uncertainty covariance matrix is also updated to reflect the growing uncertainty in the prediction.

In the correction step the predicted state is corrected based on measurements made of the system state.
The weight(significance) given to the new measurement is a combination of the uncertainty of the prediction and the uncertainty in the measurement.
These uncertainties are quantified with an assumed normal distribution with zero mean value.
These uncertainties are then used to calculate the optimal weights to give to each correction in order to bring the estimated state to the true state is the shortest amount of time.



State space
~~~~~~~~~~~~~~~~

To use a kalman filter a discrete state space model is needed.
This model describes the the evolution of the states of the system from one frame to the next.

The system is on the form:

.. math::
    x_k = A\,x_{k-1} + B\,u_k + Q_k

.. math::
    y_k = C\,x_k + D\,u_k + R_k

Where:

+--------+------------------------------------+----------------+
| Symbol |             Description            | Dim of element |
+--------+------------------------------------+----------------+
|    x   |            state vector            |      n x 1     |
+--------+------------------------------------+----------------+
|    u   |            input vector            |      p x 1     |
+--------+------------------------------------+----------------+
|    z   |            output vector           |      q x 1     |
+--------+------------------------------------+----------------+
|    A   |            state matrix            |      n x n     |
+--------+------------------------------------+----------------+
|    B   |            input matrix            |      n x p     |
+--------+------------------------------------+----------------+
|    C   |            output matrix           |      q x n     |
+--------+------------------------------------+----------------+
|    D   |          feedtrough matrix         |      q x p     |
+--------+------------------------------------+----------------+
|    Q   |    model uncertainty covariance    |      n x n     |
+--------+------------------------------------+----------------+
|    R   | measurement uncertainty covariance |      q x q     |
+--------+------------------------------------+----------------+

The subscript k and k-1 denoted the present and previous time steps.


Algorithm
~~~~~~~~~~~~~~~~

Like mentioned previously the kalman filter can be broken down into to steps.

Prediction:

.. math::
    x_{k|k-1} = A\, x_{k-1|k-1} + B\,u_k

.. math::
    P_{k|k-1} = A\, P_{k-1|k-1}\, A^T + Q_k

Correction:



.. math::
    K = (P_{k|k-1}\,C^T)(C\, P_{k|k-1}\, C^T + R_k)^{-1}

.. math::
    y_k = z_k - C\,x_{k|k-1}

.. math::
    x_{k|k} = x_{k|k-1} + K\, y_k

.. math::
    P_{k|k} = (I-K\, C)\, P_{k|k-1}

Where :math:`K` is the optimal kalman gain and :math:`z_k` is the current measurement of one or more of the states and :math:`P_k` is the state uncertainty covariance.


Extended filter
~~~~~~~~~~~~~~~~

In the case of a non linear system, the filter state estimation algorithm can still be used.
However this is given that the system can be linearized and is reasonably linear around the point at which the system is linearized.

Given this a state transition equation and a state measurement equation can be written on the from:

.. math::
    x_k = f(x_{k-1}, u_k) + Q_k

.. math::
    y_k = h(x_{k-1}) + R_k

The linear system now becomes the jacobian of the above equations evaluated at the previously estimated system state.

.. math::
    A_k = \frac{\delta f(x_{k-1}, u_k)}{\delta x} |_{x_{k-1}}

.. math::
    B_k = \frac{\delta f(x_{k-1}, u_k)}{\delta u} |_{x_{k-1}}

.. math::
    C_k = \frac{\delta h(x_{k-1})}{\delta x} |_{x_{k-1}}



Asynchronous filter
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In many practical applications the measurement data (or parts of it) is not always accessible for each iteration of the filter.
In these cases a filter can be said to be asynchronous. That is, the prediction is computed each step, then once measurement data is available the associated states are corrected.

This requires that a measurement model is created for each of the separate measurement sensors (some times clustered), so that they can be individually updated and corrected.
