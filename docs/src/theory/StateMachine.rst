
State machine
----------------

A state machine is a model for the behavior of a system. Where the system is drawn as a set of states and the transitions that take the system from one state to the other.
The states are often drawn as ellipses and the transitions are drawn as arrows connecting the states in the system. [SM]_

.. figure:: /figs/Theory/StateMachine.png
    :align: center
    :width: 600px

    A simple state machine showing the different states of a game and the transitions between states