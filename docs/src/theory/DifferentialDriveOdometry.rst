Differential drive
---------------------------------

A differential drive system is a method for locomotion where two wheels either side of the robot are independently controlled to achieve forwards and turn manuevers.

To describe its motion the following equations can be set up:

.. math::
    \dot{x} = \frac{v_r+v_l}{2}

.. math::
    \omega = \frac{v_r-v_l}{b}

Where v with subscript l and r denotes the left and right wheel velocities, and b is the wheel base distance

In matrix from:

.. math::
    \begin{bmatrix}
    \dot{x} \\ \omega
    \end{bmatrix} = 
    \begin{bmatrix}
    \frac{1}{2} & \frac{1}{2} \\
    \frac{1}{b} & -\frac{1}{b}
    \end{bmatrix} *
    \begin{bmatrix}
    v_r \\ v_l
    \end{bmatrix} =>
    \begin{bmatrix}
    v_r \\ v_l
    \end{bmatrix} = 
    \begin{bmatrix}
    \frac{1}{2} & \frac{1}{2} \\
    \frac{1}{b} & -\frac{1}{b}
    \end{bmatrix}^{-1} * 
    \begin{bmatrix}
    \dot{x} \\ \omega
    \end{bmatrix}

Here we have a method for calculating the desired wheel velocity based on desired movements of the robot.

.. figure:: /figs/Theory/DiffDrive.png
    :align: center

    A differential drive robot and its actuation principle.