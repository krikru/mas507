Product development
----------------------------

The product development was divided into two phases

1. Concept generation and evaluation
2. Contiunous improvement

.. figure:: ../figs/Theory/ConceptPhase.png
    :width: 100%

In the first phase, several different concepts for solving the task is made, then the concepts are evaluated and the best concept is chosen for further development.

The second phase integrates with the testing of the system, any flaws in the design is fixed, and in some cases it may be found that the chosen concept has a flaw that was not accounted for in phase one. If so, the concepts can be reevaluated taking into account the new findings.