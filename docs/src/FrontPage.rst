Front page
==============

MAS-507 Product Development and Project Management.
----------------------------------------------------------

This report covers the development procedure for the autonomous strawberry harvester BARTOSH.


Abstract
------------------------
An indoor localization system is developed, implemented as an extended Kalamn filter using AruCo markers as absolute land marks.
Further a strawberry harvesting and storage device is developed for the specified task. The robot body is also further developed to encapsulate and make the robot more human friendly.
A scalable control algorithm is developed to drive the robot from one bush to the next autonomously.
All the code developed is designed with a vision for a high degree of reusability and modularity.

The system is lastly tested and is able to keep track of its location, even when no land mark is in sight. Navigate from bush to bush, and detect whether a berry is ready for harvesting. The harvester is also able to carefully release the berries from the bushes and place them in the storage hopper.



Group members:
------------------------

**Group number:** 5

| **Marius Egeland**
| Mechatronics fjomp
| marius-egeland@hotmail.com

| **Kristoffer Hansen Kruithof**
| Titles titels
| Email at email

| **Harald Sangvik**
| Titles titels
| Email at email

.. figure:: /figs/product-development/BerryPicker.jpg
    :scale: 40%
    :align: center

    The final design of the robot, placed on the simulated strawberry-field

Lecturers
------------------------

| **Sondre Sanden Tørdal, PhD**
| Postdoctoral Fellow in Mehcatronics
| https://www.uia.no/kk/profil/sondrest

| **Mette Mo Jakobsen, PhD**
| Professor in Product Development and Project-based learning
| https://www.uia.no/kk/profil/mettemj
