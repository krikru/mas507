Harvester concepts
----------------------------------------------


Concepts considered
~~~~~~~~~~~~~~~~~~~~~~

The jetbot frame was already provided, so all of the concepts were made to bolt on to the Jetbot frame.

1. A 2 axis robot arm for picking berries.
2. A combined scoop and container for picked berries.
3. A "plow".
4. Separate container and scoop.
5. Same as 4, with a wider scoop.

.. figure:: /figs/product-development/RobotArmConcept.jpg
    :width: 100%
    :alt: Concept1

    Two axis robot arm

    This concept can pick strawberries in any location, but requires very good positioning.

.. figure:: /figs/product-development/Konsept1.JPG
    :width: 100%
    :alt: Concept2

    Hopper with integrated scoop.
	
This concept may have problems with strawberry capacity, as well as reach. Another issue is that the berries can be squished in the mechanism since the arm is moving inside the hopper.

.. figure:: /figs/product-development/Plow.JPG
    :width: 100%
    :alt: Concept3

    Strawberry plow

The strawberry plow use no servos and no robot arm, but can only pick strawberries close to the ground, and can not get close to the bush.
	
.. figure:: /figs/product-development/Konsept2.JPG
    :width: 100%
    :alt: Concept4

    Hopper with scoop
	
The robot could be shortened a bit, and the required positioning accuracy could be reduced by changing the dimensions of the hopper. This improvement resulted in the final concept:
	
.. figure:: /figs/product-development/Konsept3.JPG
    :width: 100%
    :alt: Concept5

    Bigger scoop

Concept evaluation
~~~~~~~~~~~~~~~~~~~~


+------------------------+--------------------------------------------------+------------------+------+----------------+-----+-----+-------+
|                        |                                                  |                  |             **Concept number**            |
+------------------------+--------------------------------------------------+------------------+------+----------------+-----+-----+-------+
| **Criterion**          | **Wanted quality**                               |**Weight (0-1)**  | **1**| **2**          |**3**|**4**|**5**  |
+------------------------+--------------------------------------------------+------------------+------+----------------+-----+-----+-------+
|                                                                                                                                          |
+------------------------+--------------------------------------------------+------------------+------+----------------+-----+-----+-------+
| Easy assembly          | Easy to assemble, few parts                      | 0.3              | 3    | 5              | 10  | 5   | 5     |
+------------------------+--------------------------------------------------+------------------+------+----------------+-----+-----+-------+
| Material usage         | Printed plastic, number of servos                | 0.5              | 3    | 4              | 7   | 5   | 4     |
+------------------------+--------------------------------------------------+------------------+------+----------------+-----+-----+-------+
| Strawberry capacity    | Should hold at least 8                           | 1                | 5    | 3              | 2   | 5   | 6     |
+------------------------+--------------------------------------------------+------------------+------+----------------+-----+-----+-------+
| Functionality          | Ability to pick strawberries, required precision | 0.8              | 4    | 5              | 2   | 5   | 6     |
+------------------------+--------------------------------------------------+------------------+------+----------------+-----+-----+-------+
|                        |                                                  |                  |      |                |     |     |       |
+------------------------+--------------------------------------------------+------------------+------+----------------+-----+-----+-------+
|                        |                                                  | Weighted sum:    | 10.6 | 10.5           | 10.2| 13  | 14.3  |
+------------------------+--------------------------------------------------+------------------+------+----------------+-----+-----+-------+

Concept number 5 is evaluated as the best concept, due to having the highest chance of picking a berry even if the obtainable positioning accuracy is low.
This concept was printed and mounted to the Jetbot.

