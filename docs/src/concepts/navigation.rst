Indoor navigation
----------------------------------------

Navigating an environment can be done in a multitude of different ways.

From advanced path planing using cost optimization techniques, to simple commands like driving in straight lines, then turn, then drive again.

The latter concept is the concept of choice for the robot, that is because the movement from one plant to another plant is mainly a straight line.
Once at the plant a turn is required to face the berry.

Therefore the simplest of the mentioned concept are chosen.
