Indoor localization
----------------------------------------

Localizing the robot in the environment can be accomplished using several different techniques.

Using the OpenCV libraries and a bit of code, the robots position can be estimated from the ArUco markers using the on-board camera on the Jetbot.

From the commanded wheel velocities and a kinematic model of the differential drive system the robots position can be found by integrating the velocities over time.

Using an inertial measurement unit the robots position can be found by integrating the linear accelerations and the angular rates, giving an estimated position.


A different simpler form of localization is simply driving until a known marker is at a set distance from the robot, this is not so much localization as it is an execution of an instruction.
But it is included here as it is a viable form of "navigating" the world given knowledge of the markers locations.

Chosen localization concept
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The chosen concept for localizing the robot is on that incorporates all the above elements.

A kalman filter is used to fuse the different estimates of the system.

The camera is used to determine the absolute position of the robot, and correct divinations from the other sensors.

The commanded wheel velocities along with a kinematic model of the robot is used to estimate the next system state.

An IMU (inertial measurement unit) is used to improve and correct the estimated states.

The below figure depicts the world map for the EKF, the ArUco markers are used as absolute position references.

.. figure:: /figs/methods/WorldMap.png
    :align: center

    The world map for the robot (Actual placements of ArUco markers was altered slightly in final code, changes not reflected in the map)

.. figure:: /figs/methods/LocalFrameRobot.png
    :align: center
    :width: 600px

    The local frame of the Jetbot, with its origin in the middle of the wheelbase.