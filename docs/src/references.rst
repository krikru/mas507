References
============


.. [Repo] Gitlab repo https://gitlab.com/krikru/mas507

.. [ImSeg] Image segmentation https://en.wikipedia.org/wiki/Image_segmentation

.. [HSV] HSV & HSL colour representation https://en.wikipedia.org/wiki/HSL_and_HSV

.. [ROS] ROS homepage https://www.ros.org/

.. [IMU] Inertial Measurement Unit https://en.wikipedia.org/wiki/Inertial_measurement_unit

.. [SM] Finite state machine https://en.wikipedia.org/wiki/Finite-state_machine

.. [FB360] Feedback 360 servo motor datasheet https://www.pololu.com/file/0J1395/900-00360-Feedback-360-HS-Servo-v1.2.pdf

.. [Kalman-filter] Kalman filter https://en.wikipedia.org/wiki/Kalman_filter

.. [Bang-Bang] Bang bang controller https://en.wikipedia.org/wiki/Bang%E2%80%93bang_control