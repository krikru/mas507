Detection from camera
----------------------------

The camera system gives stable detection of strawberry postition as well as ArUco position and orientation. This forms the basis for the Jetbot to localize itself in the global frame and identify which berries to pick.

.. figure:: /figs/result/ArUcoDetection.png
    :align: center
    :width: 600px
    
    Detection of ArUco position and Orientation

.. figure:: /figs/result/StrawberryDetection.png
    :align: center
    :width: 600px
    
    Detection of strawberry position

Positioning system
------------------------

The Pose EKF works well to estimate the robot position in the global frame, and keeps a usable estimate of the Jetbot position for a while after the ArUco marker is no longer in view.

.. figure:: /figs/result/ArucoLocalization.png
    :align: center
    :width: 600px

    Aruco marker detection and EKF output of the robot pose

Validations of requirements
--------------------------------------

The robot can navigate autonomously in the strawberry-field and completes the harvest in less than 3 minutes as per task specification. 

.. figure:: /figs/product-development/BerryPicker.jpg
    :scale: 40%
    :align: center

    The robot on it's way to pick strawberries

