Discussions & Conclusion
==================

Camera choice
-----------------

As the camera is quite wide-angled it makes the image quite distorted towards the edges, choosing a "flatter" camera with a more narrow field of view (FoV) might have made ArUco detection better in the outer regions of the frame. Using a more narrow FoV camera the image cropping for ArUco detection would probably not be necessary.


Scoop design
------------------

The arrived at concept for the strawberry harvesting actuator might work well with berries that are stuck to the plant using magnets. but for a real world application "pulling" the strawberries from the plant might harm or somehow squish the berries. But the design works well for the simulated environment.


Motor feedback
-------------------

The servo motors used by the robot has feedback, but it has not been connected to the robot. The motor outputs the rotor position according to the datasheet [FB360]_. Using this feedback from the motors would greatly improve the position estimate from the EKF when not seeing an ArUco marker. 


GPU acceleration
--------------------

It would be interesting to try and implement GPU-acceleration of the matrix operations done in some of the calculations for the EKF in particular. The Jetson has an on-board GPU with tensorflow capabilities that are as of now unused in the calculations.



Conclusion
----------------

The final product is a robot which meet the set requirements given in the task. The group is pleased with the performance, design and modularity of the robot.

.. figure:: /figs/result/bartosh.jpg
    :align: center
    :width: 600px

    A happy Bartosh

   