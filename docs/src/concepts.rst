Concepts
======================

The developed concept for the platform was quickly seen as a Barely Autonomous Robot To Optimize Strawberry Harvesting.

Therefor given the project name **BARTOSH**

.. include:: concepts/localization.rst

.. include:: concepts/navigation.rst

.. include:: concepts/scoopConcept.rst



