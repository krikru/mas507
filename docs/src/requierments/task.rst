

* The robot shall be able to identify and pick strawberries.
* It should be able to discern between ripe and unripe berries.
* There must be room for carrying 8 berries.
* The time to complete the track should be under 3 minutes

The robot will follow a track where aruco-markers and berry bushes are placed. It should navigate the track and pick the ripe strawberries autonomously.

.. figure:: ../figs/product-development/strawberryField.png
    :scale: 70%
    :alt: Strawberry field
    :align: center

    The strawberry field where the robot will operate