Autonomous navigation
======================

Part of the task is getting the robot to navigate around the strawberry-bushes autonomously.

Autonomous navigation can be broken down into two main problems:

* Where am I? 
* Where am I going?

Extended Kalman filter
----------------------

For the first problem - estimating the robots pose - an extended Kalman filter has been implemented.
The base estimation that goes on in the filter is updating the position based on dead-reckining odeometry using the input given to the robot wheels.
Dead-reckoning odeometry will quite quickly drift away from actual position in this case, as the wheels are running without feedback - meaning the position is estimated purely based on a guess of how the motors behave for a given inpout.
The Kalman filter will get measurements from an Inertial Measurement Unit (IMU) and visual odeometry from the camera in order to update and improve the estimate of the robot pose.

(Mer om kalman-filteret)


Improving estimation
---------------------

Estimation from the Kalman filter will be updated utilizing an IMU and visual odeometry from the connected camera based on ArUco markers placed around the strawberry-bushes.

Connecting an IMU to the robot.
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Connecting an IMU to the robot will allow for better estimation of the robot position as it will give us an instantaneous, albeit noisy measurement of the turning-rate and linear acceleration of the robot.
As the motors are running without feedback it is assumed that they run in unison - this might not be the case. If there is an offset in the motor center-point a command to send the robot in a straight line forward might result in an unintended turning-rate.
The gyroscope can be used to pick up on the unintended turning-rate resulting from this offset, and can also be used to correct the pose of the robot should the turning-rate command give inaccurate results when integrating from the dead-reckoning estimation. 

