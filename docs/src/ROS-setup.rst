ROS Setup
===============

ROS for multiple machines
--------------------------

For reference, the master machine is the one running roscore, and the other machines on the network will be referred to as slaves in this section.
Setting up ROS for multiple machines requires defining a couple of parameters for both master and slave machines, namely ROS_MASTER_URI and ROS_IP.

On the slave machines it is required to define ROS_MASTER_URI and set it to the IP address and gate of the machine running the roscore. It is also required to set ROS_IP to the IP of the local machine.
The slave will be known on the ROS network as whatever it has set it's ROS_IP to, and will only be able to register topics to the remote master if ROS_MASTER_URI is set.

On the master you only have to set the ROS_IP parameter to the IP of the local machine.

Setting these parameters are done at the bottom of the .bashrc file located at "~" (/home/<username>), this file runs every time a terminal-instance is created.

**Note:** In our case, the firewall had to be disabled on the slave computer in order for the data to be published to the registered topics.
The firewall can be disabled by issuing the command: "sudo ufw disable" in the terminal window on the slave machine.

* `Running ROS on multiple machines <http://wiki.ros.org/ROS/Tutorials/MultipleMachines>`_

.. figure:: /figs/ROS-setup/bashrc-slave-machine.png
    :alt: Example of the .bashrc file on the slave machine

    Example of the .bashrc file on the slave machine with ROS_MASTER_URI and ROS_IP defined

