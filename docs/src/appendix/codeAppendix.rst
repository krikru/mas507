Code
-------------------------------------

JetbotCameraLo(Hi)Res.py
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../src/JetbotCameraLoRes.py
    :language: python


JetbotController.py
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../src/JetbotController.py
    :language: python


JetbotDriver.py
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../src/JetbotDriver.py
    :language: python


JetbotStateMachine.py
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../src/JetbotStateMachine.py
    :language: python

MarkerEqkf.py
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This code was tested and found to be to computationally heavy.

Reference to source in code

.. literalinclude:: ../../src/MarkerEqkf.py
    :language: python

PoseEKF.py
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../src/PoseEKF.py
    :language: python

StrawberryDetector.py
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: ../../src/StrawberryDetector.py
    :language: python
