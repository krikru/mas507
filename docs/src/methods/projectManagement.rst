Project management
-------------------------

Trello
~~~~~~~~~~~~~~~~~~~~~~

Trello is a online Kanban-style tool for project management. Tasks are created as cards and the cards are collected under named columns to describe for instance what state of development the task is currently in. Tools like Trello are very good for managing bigger projects with many contributors. As the group consists of three members task delegation and reviewing the progress of the tasks have been quite easy. The group usually work together in the same room on campus, so progress meetings have been done ad hoc.

Trello was used most actively in the beginning of the project to get an active start and create an overview of the tasks that needed to be done.

.. figure:: /figs/methods/TrelloBoard.png
    :align: center
    :width: 600px

    The Trello board created for the project.

Git
~~~~~~~~~~~~~~~~~~~~~~

Git is a distributed version-control system (DVCS) and was actively used during the entire project period. `GitLab <https://about.gitlab.com/>`_ was used as the project repository containing all the software created for the project. The repository was forked from the main `MAS507 repo <https://gitlab.com/uia-mekatronikk/mas507>`_ and expanded on with software written by the group.

Git has also worked as the main way to deploy software to the Jetbot. All software written has been pushed to GitLab before being pulled to the Jetbot, eliminating the need for USB-sticks and the like to transfer the files.


Scrum and Agile
~~~~~~~~~~~~~~~~~~~~~~~~

Though not a classical scrum approach has been adhered to fully, elements of the methodology has been used.

* Most of the work done have been planed and talked over with the team before a single team member starts implementing a new feature, assimilating the planning phase.
* During implementation many ad hoc meetings have been held during work sessions, assimilating the daily scrum.
* During work sessions on complex coding problems a paired programming approach have been utilized allowing one programmer to focus on the implementation, and another programmer to observe and aid in problem solving and navigate future problems.
* Trough the use of Git, the code has been continently updated, and new updates have been reviewed continuously by the team.
* Lessons learned have been shared and ad hoc retrospective meetings have been held to learn from shortcomings, these lessons learned have been used in planning of further work.

.. figure:: /figs/methods/the-agile-scrum-framework-1.png
    :align: center

    Typical Scrum framework, (source: https://www.visual-paradigm.com/scrum/why-scrum-difficult-to-master/)

