.. _Ros_topics:


ROS topics
---------------------------------------

Here is a list of the topics being used to communicate on the ROS-network, the specific entries and data types can be found by inspecting the messages in the GitLab repo.

sens/velSentToWheels
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
| Topic to communicate the actual velocity commanded to the wheels, back calculated from the commanded tics rounded to the nearest integer value
| **Published by:** Jetbot Controller
| **Subscribed to by:** Pose EKF
| **Msg:** VelocityCommand

sens/measurement/imu
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
| Topic to publish data from the IMU
| **Published by:** IMU node
| **Subscribed to by:** Pose EKF
| **Msg:** Twist

sens/measurement/cam
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
| Topic to send ArUco marker information
| **Published by:** Jetbot Camera
| **Subscribed to by:** Pose EKF
| **Msg:** ArucoMarker

sens/strawberry/polar
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
| Topic to communicate strawberry position and angle relative to robot frame
| **Published by:** Jetbot Camera
| **Subscribed to by:** Jetbot Driver
| **Msg:** Polar

sens/strawberry/detection
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
| Topic to communicate if a strawberry is detected within the thresholds or not
| **Published by:** Jetbot Camera
| **Subscribed to by:** Jetbot state machine node
| **Msg:** Detection

ekf/state/pose
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
| Topic to communicate robots pose in the environment
| **Published by:** Pose EKF
| **Subscribed to by:** Jetbot Driver
| **Msg:** RobotPose2D

ekf/state/vel
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
| Topic to communicate robots velocity in the robots local frame
| **Published by:** Pose EKF
| **Subscribed to by:** (No subscribers)
| **Msg:** RobotPose2D

ekf/state/locPose
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
| Topic to communicate relative changes in robot frame since last reset of local changes
| **Published by:** Pose EKF
| **Subscribed to by:** Jetbot Driver
| **Msg:** RobotPose2D

image_raw
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
| Sends the raw image from the camera
| **Published by:** Jetbot Camera
| **Subscribed to by:** Web server
| **Msg:** Image

image_calibrated
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
| Sends the calibrated image from the camera
| **Published by:** Jetbot Camera
| **Subscribed to by:** Web server
| **Msg:** Image

image_marker
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
| Sends the calibrated image with ArUco marker detections drawn on
| **Published by:** Jetbot Camera
| **Subscribed to by:** Web server
| **Msg:** Image

strawberry_detection
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
| Sends the calibrated image with berry detection circle on it
| **Published by:** Jetbot Camera
| **Subscribed to by:** Web server
| **Msg:** Image

state/strawberry/detection
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
| Topic to communicate if a strawberry is detected within thresholds or not
| **Published by:** Jetbot Camera
| **Subscribed to by:** Jetbot state machine node
| **Msg:** Detection

state/controller/isDone
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
| Topic to communicate that Jetbot driver node is done executing commands
| **Published by:** Jetbot Driver
| **Subscribed to by:** Jetbot state machine node
| **Msg:** EmptyMsg

state/driver/setpoint
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
| Topic to communicate commands for Jetbot driver to execute
| **Published by:** Jetbot state machine node
| **Subscribed to by:** Jetbot Driver
| **Msg:** DriverSetpoint

state/reset/locPose
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
| Topic to reset local pose estimate of ekf node, subscriber ignores msg data, simply sending a msg on topic resets estimate
| **Published by:** Jetbot state machine node
| **Subscribed to by:** Pose EKF
| **Msg:** EmptyMsg

controller/velocityCommand
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
| Topic to communicate linear and angular velocities for Jetbot controller to execute
| **Published by:** Jetbot Driver
| **Subscribed to by:** Jetbot Controller
| **Msg:** velocityCommand

controller/armCommand
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
| Topic to command desired arm position
| **Published by:** Jetbot state machine node
| **Subscribed to by:** Jetbot Controller
| **Msg:** ArmCommand

joy
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
| Topic to send data from game pad controller
| **Published by:** Joy node (node running on separate machine on the network)
| **Subscribed to by:** Jetbot Controller
| **Msg:** joy



