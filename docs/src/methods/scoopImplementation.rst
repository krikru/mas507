Harvester implementation
------------------------------

Rapid prototyping
~~~~~~~~~~~~~~~~~~~~~~

The mechanical design of the robot used rapid prototyping extensively. 3D printing allows us to test and evaluate concepts quickly, making the product evolve at a fast rate as problems are discovered during testing.

.. figure:: ../figs/product-development/RapidPrototyping.jpg
    :width: 100%
    :align: center

    3d printing of parts


Continuous improvement
~~~~~~~~~~~~~~~~~~~~~~~~~~

A desire to have more protection for the electronics, as well as wanting more reach for the scoop led to a redesign of the jetbot frame.
The general idea from concept 5 is kept and further improved on.

.. figure:: ../figs/product-development/Radius.png
    :width: 100%

    Reach can be improved by moving the mounting point of the arm

*Further evolutions of the jetbot:*

.. image:: ../figs/product-development/CyberBot1.JPG
   :width: 49%
.. image:: ../figs/product-development/CyberBot2.JPG
   :width: 49%
.. image:: ../figs/product-development/CyberBot3.JPG
   :width: 49%
.. image:: ../figs/product-development/CyberBot4.JPG
   :width: 49%
.. image:: ../figs/product-development/CyberBot5.JPG
   :width: 49%
.. image:: ../figs/product-development/CyberBot6.JPG
   :width: 49%


Redesigning the robot frame allowed us to move the scoop attachment further up and back, increasing the reach of the scoop.
Attachment points for the camera and other equipment as well as a handle for easier transport of the robot was added.
The scoop has also been redesigned numerous times when testing found the design to be unsatisfactory.