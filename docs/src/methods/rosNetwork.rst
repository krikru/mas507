ROS
----------------------------------------------

ROS network
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

In the below figure the entire ROS-network is plotted using rqt_graph on a connected laptop. Rqt_graph plots all nodes and messages in the ROS-network to give a graphical overview of how all nodes are connected and which nodes publish and subscribe to each topic.

| **Note:**
| Rqt_graph only plots topics with subscribers, so all topics are not present in the overview. For example the web_video_server only subscribes to the image topics if someone is connected in a browser and is viewing a stream, so these are not present on the graph by default.


.. figure:: /figs/methods/RosNetwork_crop.png
    :align: center
    :width: 600px

    Overview of the ROS-network during normal operation as output from rqt_graph on a connected laptop

An overview of the different topics on the network can be found in the :ref:`Ros_topics` section, and a detailed description of what each node does can be found in the :ref:`Ros_nodes` section.

ROS workspace
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The IMU node is the only ROS-node in the package written in C++, and required some additional edits of the package.xml and CMakeLists.txt that are not required with python nodes. As the node is using a custom library it has to be defined in the CMakeList for catkin to know where to look for the source-files and what to name the library. 

As C++ files need to be compiled before they can be run, the node itself must also be defined in the CMakeList. This means that if it is desired to change any of the settings on the accelerometer or gyroscope, or if you want to change the publishing-rate of the node - catkin_make must be run in order for these changes to take effect.

All user-defined messages need to be specified in the CMakeList in order for these to be available to the nodes running on the Jetbot.


| **Launching BARTOSH:**
| A launch file called "Bartosh.launch" has been made to make starting the robot easy, all that is required to do to start the robot is issue the command "roslaunch mas507 Bartosh.launch" in the command line after connecting to the robot via ssh.


ROS for multiple machines
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For reference, the master machine is the one running roscore, and the other machines on the network will be referred to as slaves in this section.
Setting up ROS for multiple machines requires defining a couple of parameters for both master and slave machines, namely ROS_MASTER_URI and ROS_IP.

On the slave machines it is required to define ROS_MASTER_URI and set it to the IP address and gate of the machine running the roscore. It is also required to set ROS_IP to the IP of the local machine.
The slave will be known on the ROS network as whatever it has set it's ROS_IP to, and will only be able to register topics to the remote master if ROS_MASTER_URI is set.

On the master you only have to set the ROS_IP parameter to the IP of the local machine.

Setting these parameters are done at the bottom of the .bashrc file located at "~" (/home/<username>), this file runs every time a terminal-instance is created.

**Note:** In our case, the firewall had to be disabled on the slave computer in order for the data to be published to the registered topics.
The firewall can be disabled by issuing the command: "sudo ufw disable" in the terminal window on the slave machine (running Ubuntu 18.04 LTS).

More info on running ROS on multiple machines can be found on the ROS wiki: `"http://wiki.ros.org/ROS/Tutorials/MultipleMachines" <http://wiki.ros.org/ROS/Tutorials/MultipleMachines>`_ 

.. figure:: /figs/ROS-setup/bashrc-slave-machine.png
    :alt: Example of the .bashrc file on the slave machine
    :align: center

    Example of the .bashrc file on the slave machine with ROS_MASTER_URI and ROS_IP defined

