.. _Ros_nodes:



ROS nodes
------------------------------

This chapter describes in detail how the different ROS-nodes running on the Jetbot work.

The picture below describes what the different graphical elements mean for the overview-diagrams of the ROS-nodes.

.. figure:: /figs/methods/WhatDoesItMean.png
    :align: center

    Description of the graphical elements in the ROS-node diagrams

IMU node
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The IMU-node gets the acceleration and angular rates from the GY-85 IMU connected to the I2C-bus of the Jetbot and publish it onto the ROS-network under the topic "sens/measurment/imu". The node uses functions from a library written by amaork named libi2c found on `github <https://github.com/amaork/libi2c>`_ to issue commands to the I2C-bus on the Jetson. The IMU-data is polled and published at a rate of 25 Hz.

The accelerometer is setup to output the linear accelerations with a magnitude of :math:`\pm 2g` and the gyroscope ouputs the rate at :math:`\pm 2000 \ \frac{deg}{s}` (Gyro output magnitude locked by manufacturer). 



Kalman node
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The kalman filter node is the main positioning element in the ROS-network, this node takes in information from the different sensors and fuse it into an estimate of the robot pose in the global frame.

.. figure:: /figs/methods/EKFNode.png
    :align: center
    :width: 600px

    Block diagram overview of the objects in the EKF node

To implement the kalman filter a mathematical description of the Jetbot dynamics is needed.
Along with the physical model a measurement model is needed for each sensor.

| **Physical model**
| Setting up the state transition function:

.. math::
    x_k = f(x_{k-1}, u_k) + Q_k

| Where the state vector is:

.. math::
    x = \begin{bmatrix}
    x_g & y_g & \dot{x_l} & \ddot{x_l} & \ddot{x_b} & \theta_g & \omega_l & \dot{\omega_l} & \omega_b
    \end{bmatrix}^T

| And the state transition function is:

.. math::
    f(x_{k-1},u_k) = \begin{bmatrix}
    x_g + x_l * cos(\theta_g) * \Delta t \\
    y_g + x_l * sin(\theta_g) * \Delta t \\
    \dot{x_l} + \ddot{x_l} * \Delta t \\
    \frac{1}{\tau_x}*u_x - \frac{1}{\tau_x}*\dot{x_l} \\
    \ddot{x_b} \\
    \theta_g + \omega_l * \Delta t \\
    \omega_l + \dot{\omega_l} * \Delta t \\
    \frac{1}{\tau_{\omega}}*u_{\omega} - \frac{1}{\tau_{\omega}}*\omega_l \\
    \omega_b
    \end{bmatrix}

| Where :math:`g` denotes the global coordinates, :math:`l` denotes local coordinates, and :math:`b` denotes sensor biases.

| This function is linearized and implemented in the prediction model of the kalmen filter

| **Measurement models**
| There are two measurement models, one for the camera and one for imu mounted on the frame.

| The imu measurement model is fairly simple:

.. math::
    \ddot{x}_m = \ddot{x}_l + \ddot{x}_b

.. math::
    \omega_m = \omega_l + \omega_b

| The camera measurement model is simple once the global coordinates are computed from the camera data:

.. math::
    x_m = x_g

.. math::
    y_m = y_g

.. math::
    \theta_m = \theta_g

| In the above equations :math:`m` denotes measured data
| Obtaining the global coordinates from the camera data is done using the following equation:

.. math::
    H^G_M = H^G_R * H^R_C * H^C_M

| Where, :math:`G` is the global frame, :math:`M` is the marker frame, :math:`R` is the robot frame and :math:`C` is the camera frame.

| Rearranging the above equations gives:

.. math::
    H^G_R = H^G_M *(H^R_C * H^C_M)^{-1}

| The matrix from the robot frame to the camera frame can be found experimentally, the markers location in the global frame is known and the transformation from the camera to the marker is measured using the camera and functions from the openCV library.

| Once the matrix from the ground frame to the robot frame is found we need to obtain the robots coordinate from the matrix, the :math:`X` and :math:`Y` location can be read straight from the matrix.

The angle theta is a bit more involved but is solved in the following manner.

| The rotation matrix can be considered to be on the form of a pure rotation around the Z axis.

.. math::
    R^G_R = \begin{bmatrix}
    r_{11} & r_{12} & r_{13} & \\
    r_{21} & r_{22} & r_{23} & \\
    r_{31} & r_{32} & r_{33} &
    \end{bmatrix} = 
    \begin{bmatrix}
    cos(\theta) & -sin(\theta) & 0 & \\
    sin(\theta) & cos(\theta) & 0 & \\
    0 & 0 & 1 &
    \end{bmatrix}

| Here it can be seen that all three row vectors making up the matrix are defined to be of unity length, and also that the bottom elements in the first two vectors are defined to be zero

| It is also known that the three vectors are supposed to be orthogonal.

| Exploiting these facts we can rescale the first two vectors after setting the bottom elements to zero.

.. math::
    \begin{bmatrix}
    r_{11} \\ r_{21} \\ 0
    \end{bmatrix} \times
    \begin{bmatrix}
    r_{12} \\ r_{22} \\ 0
    \end{bmatrix} = 
    \begin{bmatrix}
    0 \\ 0 \\ r_{33}
    \end{bmatrix}

| From this is follows that:

.. math::
    \frac{1}{\sqrt{|r_{33}|}}
    \begin{bmatrix}
    r_{11} \\ r_{21} \\ 0
    \end{bmatrix} \times \
    \frac{sign(r_{33})}{\sqrt{|r_{33}|}}
    \begin{bmatrix}
    r_{12} \\ r_{22} \\ 0
    \end{bmatrix} =
    \begin{bmatrix}
    0 \\ 0 \\ 1
    \end{bmatrix}

| Letting these new vectors form the basis for our matrix gives (new values are marked with a prime)

.. math::
    \begin{bmatrix}
    r_{11}' & r_{12}' & 0 & \\
    r_{21}' & r_{22}' & 0 & \\
    0 & 0 & 1 &
    \end{bmatrix} = 
    \begin{bmatrix}
    cos(\theta) & -sin(\theta) & 0 & \\
    sin(\theta) & cos(\theta) & 0 & \\
    0 & 0 & 1 &
    \end{bmatrix}

| Now a trigonometric identity is used:

.. math::
    tan(\theta) = \frac{sin(\theta)}{cos(\theta)} => \theta = atan\bigg(\frac{sin(\theta)}{cos(\theta)}\bigg)

| In many programing languages there is a function for atan that gives the full angle, this is often referred to as atan2.

| Now there are four ways to estimate the angle:

.. math::
    \theta_1 = atan2(r_{21}', r_{11}')

.. math::
    \theta_2 = atan2(-r_{21}', r_{22}')

.. math::
    \theta_3 = atan2(-r_{21}', r_{11}')

.. math::
    \theta_4 = atan2(r_{21}', r_{22}')

| The final estimate is the average of the four estimates above

.. math::
    \theta = \frac{\theta_1 + \theta_2 + \theta_3 + \theta_4}{4}

| **Camera and marker location**
| For camera measurement model the transformations from the global origo to the markers are needed along with the transformation from the robot frame to the cameras optical frame.

| The transformation from the global origin to the markers are based on the setup provided in the task description.

| To find the transformation from the robots local frame to the camera frame is a calibration problem. To solve for this matrix the Jetbot was placed facing a marker with a known relative position and orientation. A few lines of code in the measurement model allowed us to read out the measured transformation from the camera to the marker. The following equation was setup and used:

.. math::
    H^R_M = H^R_C * H^C_M => H^R_C = H^R_M * (H^C_M)^{-1}

.. figure:: /figs/methods/cameraCalib.jpg
    :align: center

    Picture of camera extrinsic parameter calibration


| **Theta estimation and measurement model for theta**
| The kalman filter is in many ways a standard implementation of the filter. There is one noteworthy exception. The theta estimation is limited to the range:

.. math::
    \theta \in [0,2\pi]

| This has some interesting implications for the innovation signal in the kalamn correction step. The residual needs to be calculated as the shortest path from the measurement to the estimated state. If the wrapping of the number line is not accounted for the filter will struggle around crossing the 0 and 2pi points as the estimated value and the measured value will have a large deviation, where as in reality the deviation is small.

| This is solved in the following way.

| The innovation signal is computed in three manners, the innovation with the smallest absolute value is used

.. math::
    e_1 = z_k - C*x_k

.. math::
    e_2 = 2\pi - e_1

.. math::
    e_3 = e_1 - 2\pi

.. math::
    y_k = min\{e_1,\ e_2,\ e_3\}

| The predicted value also needs to be conditioned to reflect the wrapping of the angle

.. math::
    \theta = \theta \bmod 2\pi


| **Messages completeness check**
| The incoming messages from the camera data are sometimes not complete, this issue has not been solved on the sending side. So a class is set up to check the completeness of the messages, if the message is not complete, the class returns the last complete message it accepted.

| Further work could be done here to intelligently filter the messages, the Jetsons AI capabilities could be utilized for this purpose.

| **Code implementation**
| The kalamn filter is implemented as an asynchronous filter, and corrections are computed as soon as new data is available.
| This computation is called in the callback functions of the subscribers to the sensor data.

| The predict cycle is ran each time the main loop of the program is computed, and the resulting predictions are are published to the ROS network.




Jetbot driver node
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The driver node is the node responsible for driving the robot to each setpoint and to point the robot towards the strawberry.

The node is schematically structured in the following way.

.. figure:: /figs/methods/DriverNode.png
    :align: center

    Block diagram overview of the objects in the driver node

The driver node has four modes:

1. Pure pursuit mode, this mode drives the robot to a set set-point in the global frame
2. Turn control, this mode rotates the robot to a given angle in the global frame
3. Strawberry, this mode first rotates the robot to get the strawberry aligned with the robots x_axis, then it drives so that the berry is a set distance from the robot
4. Return mode, this mode does the reverse of the movements that was commanded in mode 3.

Common for all the modes is the use of bang bang control. This is a very simple control strategy where the actuators are either fully on or fully of. The reason for using such a simple control law is that the motors are quite limited in both the response and maximum value. Also the motor set-points are ramped up and down to the desired value before being sent to the motors, so carful ramping from the controller is not needed.

Also common for the control modes is that the controller only needs to bring the robot within a certain tolerance of the desired output to stop actuating the system.
The reason for this is twofold; it is unrealistic to expect the system to reach a precise set-point, second it allows the set-point for the motors some time to ramp down, resulting in less overshoot. Once the controller reaches its set-point it sets a Done flag high, and stops actuating the system. This tolerance is denoted 'tol', and differs in the different control modes.

The modes can be visualized as follows:

.. figure:: /figs/methods/DriverNodeDiagram.png
    :align: center

    Block diagram overview of controller modes

| **Mode 1, Pure pursuit:**
| In the pure pursuit mode the Jetbot drives towards a target set in the global frame, using a homogeneous transformation this set-point is translated into the robots frame.
| The robots control law is then a simple bang bang controller for the forwards movement, and a proportional controller for the turn rate.

| The forwards bang bang controller:

.. math::
    e_x = -X_{sp},\ \ if: abs(e_x) > tol,\ \ u_x = -sign(e_x)*k_v *v_{max}

.. math::
    e_y = -Y_{sp},\ \ u_{\omega} = -sign(e_x)*e_y *k_{\omega} * \omega_{max}

| Where :math:`k`'s are controller gains and will set the control effort as a proportion of the maximum defined velocity in the forward and turn directions.

| To find the set-points in the robot a homogeneous transformation was used:

.. math::
    r^R_{SP/R} = H^R_G r^G_{SP/G}

The estimated robot position formed the basis for the homogeneous transformation.

| This transformation can be illustrated as:

.. figure:: /figs/methods/robotGlobalFrame.png
    :align: center

    Transformation of set-point


| **Mode 2, Turn control:**
| In the turn control mode the robot turns to a set-point in the global frame.
| The control law for this mode is a bang bang controller

| Calculating the error for the controller is complicated by the fact that:

.. math::
    \theta \ \in \ [0, \ 2\pi]

| This has the implication that 0 and :math:`2\pi` lives right next to each other, but a subtraction of the process value from the desired value will not reflect this. The error is calculated in the following way:

.. math::
    e_1 = \theta_{sp} - \theta_{g}

.. math::
    e_2 = 2\pi - e_1

.. math::
    e_3 = e_2 - 2\pi

| The error is then selected as the error with the smallest absolute value:

.. math::
    e_{\theta} = min \ {|e_1|,\ |e_2|,\ |e_3|}

| Once the error is calculated the control law is simple:

.. math::
    if: abs(e_{\theta}) > tol, \ \ u_{\omega} = -sign(e_{\theta})*k_{\omega} * \omega_{max}

| Again using the same conventions as in mode 1

| **Mode 3, Strawberry:**
| In this control mode the Jetbot first aims the robot at the detected berry, then it drives towards the berry.

| The control law is much the same bang bang laws as in mode 1 an 2.

| **Mode 4, Return mode:**
| This mode does the reverse sequence of mode 3, the motion executed in mode 3 is stored. It then drives backwards the same distance. Then once within the distance it turns back the same amount. This leaves the robot in the same position as it was in before driving towards the berry.


Jetbot controller node
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The Jetbot Controller node has been split into multiple objects (Python Classes) in order to make it modular. A block diagram overview of the node can be seen in the below figure.

.. figure:: /figs/methods/JetbotController.png
    :align: center

    A block-diagram overview of the objects making the JetbotController ROS-node and the subscribed ROS-topics.

The CommandSignalListener object takes in command signals from the ROS-network. The manual control is done via a gamepad connected to a separate machine on the ROS-network
and with it the user can control the speed and turn-rate of the robot in addition to moving the actuator. The gamepad is also used to toggle between modes to change between manual and
autonomous mode.

The DiffDrive object receives the setpoints for the linear speed and angular rate for the robot and translates these into the appropriate amount of tics to achieve the desired rate and velocity. This object also reports to the ROS network what tics it actually sent to the motors, this is done because there are limitations in what actuations that can be commanded. For instance you cannot command 350.6 tics, so the node rounds the number to the nearest integer value before setting the pwm to the servos and back-calculating these tics into a linear and and angular velocity to report this rate to the EKF. The is used in the EKF filter to predict where the robot will be.

The object called "TheScoop" is the robots strawberry-picking actuator, using two servos in order to lift/lower a wide comb-like scoop to rake the bush of any strawberries, and dispose of them into the front-mounted hopper on the robot.

the JetbotController object is what sends the PWM-signals to the servo-controller board located on the underside of the robot. This object has implemented saturations in order to not allow the other  objects to send invalid tic-values to the motors.

The node is modular in the sense that if you wanted to implement another form of actuation-scheme (say Ackerman steering) you only need to change out the "DiffDrive" object with some other object that takes the same inputs (:math:`\dot{x}, \ \dot{\theta}`) and translate these into whatever amount of ticks the different servos need to steer the robot as desired.




Jetbot camera node
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The camera node uses much of the supplied code for ArUco marker detection as well as the code for the strawberry detection.
Two nodes using the same implementation was set up: a low resolution node and a high resolution node. This was done in order to experiment with different resolutions.

| **Strawberry detector** 
| The strawberry detector is left mostly as given, with some minor modifications. The HSV-values for the red masking was altered to give more stable detection of the strawberries in the lighting-conditions at campus. The new HSV-values were found by copying an image of the strawberries taken with the Jetbot camera into an image-editiing software and using the colour-picker tool to inspect the HSV-values of the colour of the berries. 
| The other alteration is adding two publishers, one for wether or not there is a strawberry in the frame, and one for the polar coordinates of the strawberry in the robot frame. Some thresholding was added to allow for tuning of what counts as a detection, so in order to count as a valid detection the strawberry has to be within an estimated distance and angle from the robot. These thresholds were tuned in the testing stage so that if the robot accidentally saw a strawberry at the edge of the frame or saw parts of a strawberry through a bush it would not count as a detection .

Where the angle and radius to the strawberry (sb) is described with the following equations:

.. math:: 
    \theta_{sb} = atan(\frac{y_{sb}}{x_{sb}}) \hspace{2cm} r_{sb} = \sqrt{x_{sb}^2 + y_{sb}^2}

| **ArUco detection**
| The ArUco marker detection is done using the provided code. The Pose EKF only accepts one marker at the time, so in the case when two markers is present in cameras field of view this must be handled. The chosen strategy for this is to use the marker that is closest to the Jetbot.


| **Camera resolution and frame rate**
| Some experimenting was done with different capture-resolutions in order to see if the detection of the aruco-markers could be made more stable at higher resolutions.
| There is however a trade-off between resolution, camera frame rate and node execution rate. It was found that a good compromise was to increase the frame rate of the camera and keep the default camera resolution. Capturing frames from the camera with a higher frame rate reduces the amount of motion blur, thus allowing marker and berry detection whilst the robot is moving. It was found that a higher resolution made little difference in both marker and berry detection. Therefore the resolution was kept default and a slightly higher frame rate was chosen. The same execution rate of the node was kept to not overload the Jetbot.

| To use this higher resolution a new camera calibration and camera color calibration was needed. The different calibration data is the reason for implementing the code in two different nodes. The calibration data was moved into two different folders, and the high- and low-resolution nodes get the calibration-data from their respective folders. In the end the lower resolution camera-node was the one used as the higher framerate achievable outweighed the slightly more stable ArUco detection.

| **Cropping of the image**
| The camera is a wide angle camera and therefor it needs t be calibrated to produce a flat looking image, still after several calibration attempts it was found that the markers where falsy close to the robot when they where at the edges of the image. To avoid this bad marker detection to enter into the localization filter the image was cropped.

| The image cropping was done on the gray scale image in two steps, first cropping the image, then buffering the image. The buffering is done to keep the now cropped image the same size as the original image, it is also done to keep the drawn markers in the correct location in the image sent to the web server for viewing and validation of data.

| **Image pre-processing**
| To remove noise in the image before any marker or berry detection takes place a gaussian blur was applied to the image. This removes noise and made the detection of both berries and markers more stable.

.. figure:: /figs/methods/JetbotCamera.png
    :align: center



Jetbot state machine node
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The state machine controlling the autonomous strawberry picking has three main states, the "Driving State", the "Picking State" and the "Turning State".
It is essentially a set of nested state machines, where each of these main states are a state machine in itself, controlling modes in the Jetbot Driver node. The state machine issues commands to the driver and waits for confirmation that the desired robot pose has been reached before continuing.

The node automatically generates the set points for when to check for berries from a couple of user-defined parameters, namely the number of bushes, the distance between said bushes, and the global position of the two "lanes" of travel. The lanes of travel is the lanes the robot will try to use when moving between the plants. So if the robot is to navigate a field with more bushes the only alteration to the software needed would be to change the parameter "`self.\_nBush`" variable in the StateMachine class.

| **Drive state (S1):**
| The driving state takes the robot from one berry bush to another, navigating to a position from an auto-generated vector of positions based on the number of bushes and the distance between them. 
| A counter is incremented each time the robot enters this state to keep track of how many bushes has been visited. When the robot reaches a new coordinate, it transitions into the berry-picking state (T1).

| **Picking state (S2):**
| The picking state first turns the robot towards the bush and checks for a strawberry. 
| If a strawberry is detected the state machine signals the driver to move towards it and when the driver signals that the robot has reached the berry it sends a message telling the actuator to pick the berry.
| After the actuator has been lifted the robot backs up, after returning to the track it lowers the actuator. If the camera can still see a berry the robot repositions and tries to pick the berry again (T2), this step is repeated a maximum of four times before moving on. (This re-checking stage was disabled in the code as it caused some issues yet to be resolved as of writing this report).
| Depending on the amount of bushes the driving state has visited, one of two things happen. 
| Either it goes back into the drive state (T4) if the robot has not yet visited all the bushes or it goes into the turn state (T3) if all the bushes has been visited.

| **Turning state (S3):**
| The turning state handles the turning procedure when the robot has reached the end of the field facing the first marker, and runs a sequence of commands in order to put the robot at the top end of the field, facing downward towards the last marker. After reaching the top of the field facing down the machine transitions back into the drive-state (T5)


.. figure:: /figs/methods/JetbotStateMachine.png
    :align: center

    State machine for the autonomous strawberry picking

.. figure:: /figs/methods/JetbotStateMachineROS.png
    :align: center

    Block diagram overview of the subscribed and published ROS topics of the JetbotStateMachine
