

# Marker EQKF filter
class EqkfMarker(object):

    # EQKF filter based on Sondre Tordals work, see:
    # https://www.mic-journal.no/PDF/2017/MIC-2017-2-3.pdf
    # https://github.com/sondre1988/vision-tracker/tree/master/VisionTracker

    # Damping added to the markers, since this is a kinematic observer, predictions will tend to overshoot
    # Some dampening will help this issue

    def __init__(self, dt):
        
        # Sets rate of filter to self variable
        self._dt = dt

        # State and state covarianse matrix
        self._X_0 = np.zeros((19,1))


        # Initial position and rotation, isj a marker straight on
        self._X_0[0:3] = np.array([[0.02],[-0.05],[0.50]])
        self._X_0[9:13] = np.array([[0.19],[0.97],[0.03],[-0.07]])

        self._X = self._X_0
        
        self._P = np.zeros((19,19))

        # Observation matrix
        C = np.zeros((7,19))
        C_p = np.eye(3)
        C_q = np.eye(4)

        C[0:3,0:3] = C_p
        C[3:7,9:13] = C_q

        self._C = C

        # Model and measurment uncertanty
        q = 0.1
        self._Q = q**2*np.eye(19)

        r_t = 0.005
        r_a = 0.05
        r_arr = np.array([r_t**2, r_t**2, r_t**2, r_a**2, r_a**2, r_a**2, r_a**2])
        self._R = np.diag(r_arr)

        # Dampening of marker in rotation and translation, set as time constant of decay
        self._tau_l = 0.2
        self._tau_r = 0.2

        # How much of last acceleration to keep, must be in interval [0.0, 1.0], when dampening is used it whould be 0.0 or close to it
        self._a_l = 0.0
        self._a_r = 0.0

        # Marker id
        self._id = 0

        # Sets initial message, this is an arbitrary value, used for initilization
        self._arucoMsg = ArucoMarker()
        self._arucoMsg.tvec = [0.1, 0.1, 0.1]
        self._arucoMsg.rvec = [0.1, 0.1, 0.1]
        self._arucoMsg.id = 1

        # Guard against runaway estimation, if the counter value gets above the maximum counter
        # limit the estimation if halted until counter is reset by a new measurment, uncertanty matrix
        # is not reset, allows for faster setteling to new measurment value
        estimationTimeWindow =  2.0

        self._estsSinceLastMeasure = 0
        self._maxEstsBetweenMeasure = np.round(estimationTimeWindow/self._dt)

    # Helper functions to make life easier
    def _pow(self,a,b):
        # Function is used to vrap python function in c++ style notation, used in A matrix
        return np.power(a,b)

    def _sqrt(self,a):
        # Function is used to vrap python function in c++ style notation, used in A matrix
        return np.sqrt(a)

    def _normState(self,X):
        # Function to normelize the the quartonions of the state estimate

        # Gets the quartonion part of the estimate
        q = X[9:13]
        qNorm = np.linalg.norm(q)

        X[9:13] = q/qNorm

        return X

    def _quatToRod(self, quat):
        # Implementation of equation and derivation found at:
        # https://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToAngle/index.htm

        # Parsing data

        # Fineds the normalization constants
        qNorm = np.linalg.norm(quat)

        qw = quat[0][0] / qNorm
        qx = quat[1][0] / qNorm
        qy = quat[2][0] / qNorm
        qz = quat[3][0] / qNorm

        if qw > 0.99:
            # if you get here then angle is very close to 0 or n*2*pi, therefor angle is set to zero and axis to arbitrary axis
            angle = 0.0
            x = 1.0
            y = 0.0
            z = 0.0
        else:
            angle = 2*np.arccos(qw)

            s = np.sqrt(1-qw**2)

            x = qx / s
            y = qy / s
            z = qz / s

        # Rodrigues angle is axis * angle
        rod = angle*np.array([x,y,z])

        return rod

    def _rodToQuat(self, rod):
        # Implementation of equation and derivation found at:
        # https://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToAngle/index.htm

        # Parsing data

        # Finds rotation angle
        angle = np.linalg.norm(rod)

        # Rotation axis is then rod[i]/angle
        if angle > 0.001:
            # Guard agains devide by zero
            ax = rod[0] / angle
            ay = rod[1] / angle
            az = rod[2] / angle
        else:
            ax = 1.0
            ay = 0.0
            az = 0.0

        # Defines parts of quartonian
        qw = np.cos(angle/2)

        s = np.sin(angle/2)
        qx = ax*s
        qy = ay*s
        qz = az*s

        quat = np.array([qw, qx, qy, qz])

        # Normelizes quartonian
        qNorm = np.linalg.norm(quat)
        quat = quat / qNorm

        return quat
    # End of helper functions

    def _A(self, X):

        # Sets self data to variables with shorter names
        dt = self._dt

        # Dempening terms, set as time constants
        b_l = 1.0 / self._tau_l
        b_r = 1.0 / self._tau_r

        # How much of last acceleration to keep
        a_l = self._a_l
        a_r = self._a_r

        # Defines shape of A matrix
        A = np.zeros((19,19))

        # Propper notation
        q0 = X[9]
        q1 = X[10]
        q2 = X[11]
        q3 = X[12]

        w1 = X[13]
        w2 = X[14]
        w3 = X[15]

        w1_t = X[16]
        w2_t = X[17]
        w3_t = X[18]

        # Linearized system matrix A
        A[0, 0] = 1.0
        A[0, 3] = dt
        A[0, 6] = (dt*dt)*(1.0 / 2.0)
        A[1, 1] = 1.0
        A[1, 4] = dt
        A[1, 7] = (dt*dt)*(1.0 / 2.0)
        A[2, 2] = 1.0
        A[2, 5] = dt
        A[2, 8] = (dt*dt)*(1.0 / 2.0)
        A[3, 3] = 1.0
        A[3, 6] = dt
        A[4, 4] = 1.0
        A[4, 7] = dt
        A[5, 5] = 1.0
        A[5, 8] = dt
        A[9, 9] = dt*(q0*q1*(w1 + dt*w1_t)*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*(1.0 / 2.0) + q0*q2*(w2 + dt*w2_t)*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*(1.0 / 2.0) + q0*q3*(w3 + dt*w3_t)*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*(1.0 / 2.0)) + 1.0
        A[9, 10] = dt*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*((q0*q0)*w1 + (q2*q2)*w1 + (q3*q3)*w1 + dt*(q0*q0)*w1_t + dt*(q2*q2)*w1_t + dt*(q3*q3)*w1_t - q1*q2*w2 - q1*q3*w3 - dt*q1*q2*w2_t - dt*q1*q3*w3_t)*(-1.0 / 2.0)
        A[9, 11] = dt*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*((q0*q0)*w2 + (q1*q1)*w2 + (q3*q3)*w2 + dt*(q0*q0)*w2_t + dt*(q1*q1)*w2_t + dt*(q3*q3)*w2_t - q1*q2*w1 - q2*q3*w3 - dt*q1*q2*w1_t - dt*q2*q3*w3_t)*(-1.0 / 2.0)
        A[9, 12] = dt*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*((q0*q0)*w3 + (q1*q1)*w3 + (q2*q2)*w3 + dt*(q0*q0)*w3_t + dt*(q1*q1)*w3_t + dt*(q2*q2)*w3_t - q1*q3*w1 - q2*q3*w2 - dt*q1*q3*w1_t - dt*q2*q3*w2_t)*(-1.0 / 2.0)
        A[9, 13] = dt*q1*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(-1.0 / 2.0)
        A[9, 14] = dt*q2*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(-1.0 / 2.0)
        A[9, 15] = dt*q3*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(-1.0 / 2.0)
        A[9, 16] = (dt*dt)*q1*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(-1.0 / 2.0)
        A[9, 17] = (dt*dt)*q2*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(-1.0 / 2.0)
        A[9, 18] = (dt*dt)*q3*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(-1.0 / 2.0)
        A[10, 9] = dt*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*((q1*q1)*w1 + (q2*q2)*w1 + (q3*q3)*w1 + dt*(q1*q1)*w1_t + dt*(q2*q2)*w1_t + dt*(q3*q3)*w1_t + q0*q2*w3 - q0*q3*w2 + dt*q0*q2*w3_t - dt*q0*q3*w2_t)*(1.0 / 2.0)
        A[10, 10] = -dt*(q0*q1*(w1 + dt*w1_t)*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*(1.0 / 2.0) + q1*q3*(w2 + dt*w2_t)*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*(1.0 / 2.0) - q1*q2*(w3 + dt*w3_t)*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*(1.0 / 2.0)) + 1.0
        A[10, 11] = dt*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*((q0*q0)*w3 + (q1*q1)*w3 + (q3*q3)*w3 + dt*(q0*q0)*w3_t + dt*(q1*q1)*w3_t + dt*(q3*q3)*w3_t + q0*q2*w1 + q2*q3*w2 + dt*q0*q2*w1_t + dt*q2*q3*w2_t)*(-1.0 / 2.0)
        A[10, 12] = dt*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*((q0*q0)*w2 + (q1*q1)*w2 + (q2*q2)*w2 + dt*(q0*q0)*w2_t + dt*(q1*q1)*w2_t + dt*(q2*q2)*w2_t - q0*q3*w1 + q2*q3*w3 - dt*q0*q3*w1_t + dt*q2*q3*w3_t)*(1.0 / 2.0)
        A[10, 13] = dt*q0*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(1.0 / 2.0)
        A[10, 14] = dt*q3*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(1.0 / 2.0)
        A[10, 15] = dt*q2*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(-1.0 / 2.0)
        A[10, 16] = (dt*dt)*q0*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(1.0 / 2.0)
        A[10, 17] = (dt*dt)*q3*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(1.0 / 2.0)
        A[10, 18] = (dt*dt)*q2*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(-1.0 / 2.0)
        A[11, 9] = dt*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*((q1*q1)*w2 + (q2*q2)*w2 + (q3*q3)*w2 + dt*(q1*q1)*w2_t + dt*(q2*q2)*w2_t + dt*(q3*q3)*w2_t - q0*q1*w3 + q0*q3*w1 - dt*q0*q1*w3_t + dt*q0*q3*w1_t)*(1.0 / 2.0)
        A[11, 10] = dt*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*((q0*q0)*w3 + (q2*q2)*w3 + (q3*q3)*w3 + dt*(q0*q0)*w3_t + dt*(q2*q2)*w3_t + dt*(q3*q3)*w3_t - q0*q1*w2 + q1*q3*w1 - dt*q0*q1*w2_t + dt*q1*q3*w1_t)*(1.0 / 2.0)
        A[11, 11] = -dt*(q0*q2*(w2 + dt*w2_t)*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*(1.0 / 2.0) - q2*q3*(w1 + dt*w1_t)*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*(1.0 / 2.0) + q1*q2*(w3 + dt*w3_t)*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*(1.0 / 2.0)) + 1.0
        A[11, 12] = dt*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*((q0*q0)*w1 + (q1*q1)*w1 + (q2*q2)*w1 + dt*(q0*q0)*w1_t + dt*(q1*q1)*w1_t + dt*(q2*q2)*w1_t + q0*q3*w2 + q1*q3*w3 + dt*q0*q3*w2_t + dt*q1*q3*w3_t)*(-1.0 / 2.0)
        A[11, 13] = dt*q3*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(-1.0 / 2.0)
        A[11, 14] = dt*q0*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(1.0 / 2.0)
        A[11, 15] = dt*q1*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(1.0 / 2.0)
        A[11, 16] = (dt*dt)*q3*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(-1.0 / 2.0)
        A[11, 17] = (dt*dt)*q0*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(1.0 / 2.0)
        A[11, 18] = (dt*dt)*q1*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(1.0 / 2.0)
        A[12, 9] = dt*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*((q1*q1)*w3 + (q2*q2)*w3 + (q3*q3)*w3 + dt*(q1*q1)*w3_t + dt*(q2*q2)*w3_t + dt*(q3*q3)*w3_t + q0*q1*w2 - q0*q2*w1 + dt*q0*q1*w2_t - dt*q0*q2*w1_t)*(1.0 / 2.0)
        A[12, 10] = dt*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*((q0*q0)*w2 + (q2*q2)*w2 + (q3*q3)*w2 + dt*(q0*q0)*w2_t + dt*(q2*q2)*w2_t + dt*(q3*q3)*w2_t + q0*q1*w3 + q1*q2*w1 + dt*q0*q1*w3_t + dt*q1*q2*w1_t)*(-1.0 / 2.0)
        A[12, 11] = dt*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*((q0*q0)*w1 + (q1*q1)*w1 + (q3*q3)*w1 + dt*(q0*q0)*w1_t + dt*(q1*q1)*w1_t + dt*(q3*q3)*w1_t - q0*q2*w3 + q1*q2*w2 - dt*q0*q2*w3_t + dt*q1*q2*w2_t)*(1.0 / 2.0)
        A[12, 12] = -dt*(q2*q3*(w1 + dt*w1_t)*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*(1.0 / 2.0) - q1*q3*(w2 + dt*w2_t)*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*(1.0 / 2.0) + q0*q3*(w3 + dt*w3_t)*1.0 / self._pow(q0*q0 + q1*q1 + q2*q2 + q3*q3, 3.0 / 2.0)*(1.0 / 2.0)) + 1.0
        A[12, 13] = dt*q2*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(1.0 / 2.0)
        A[12, 14] = dt*q1*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(-1.0 / 2.0)
        A[12, 15] = dt*q0*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(1.0 / 2.0)
        A[12, 16] = (dt*dt)*q2*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(1.0 / 2.0)
        A[12, 17] = (dt*dt)*q1*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(-1.0 / 2.0)
        A[12, 18] = (dt*dt)*q0*1.0 / self._sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3)*(1.0 / 2.0)
        A[13, 13] = 1.0
        A[13, 16] = dt
        A[14, 14] = 1.0
        A[14, 17] = dt
        A[15, 15] = 1.0
        A[15, 18] = dt
        

        # Dampening, added by ME, not in the paper

        A[6, 3] = -b_l
        A[7, 4] = -b_l
        A[8, 5] = -b_l

        A[16, 13] = -b_r
        A[17, 14] = -b_r
        A[18, 15] = -b_r

        # Due to the adde dampening the acceleration therms are set to zero

        A[6, 6] = a_l
        A[7, 7] = a_l
        A[8, 8] = a_l

        A[16, 16] = a_r
        A[17, 17] = a_r
        A[18, 18] = a_r

        return A

    def predict(self):
        # Function for predicting where the robot is located
        

        # Increments counter
        self._estsSinceLastMeasure += 1

        # If we are still in the estimation time window we estimate the marker position,
        # else we reset the prediction
        if int(self._estsSinceLastMeasure) < int(self._maxEstsBetweenMeasure):
            # Predicting where the robot is by unlinear function
            X_p = self._A(self._X).dot(self._X)

            # Updating uncertanty covariance matrix
            P_p = self._A(self._X).dot(self._P.dot(np.transpose(self._A(self._X)))) + self._Q

            # Normelizes Quaternion
            X_p = self._normState(X_p)
        else:
            # Sets predicted state to initial state, keeps the uncertanty of the last valid prediction
            X_p = self._X_0
            P_p = self._P
            if int(self._estsSinceLastMeasure) == int(self._maxEstsBetweenMeasure):
                print('EQKF reset')
        
        # Setting predicted values to the output values
        self._X = X_p
        self._P = P_p

    def _measure(self, X_m,C):
        # Finds the error between the measurment and the predicted value

        return X_m - C.dot(self._X)

    def _computeKalman(self,C,R):
        # Computes kalman gain

        S = C.dot(self._P.dot(np.transpose(C))) + R

        return self._P.dot(np.transpose(C).dot(np.linalg.inv(S)))

    def _update(self,K,Y,C):
        # Updates estimate
        X = self._X + K.dot(Y)

        # Normelizes Quaternion
        X = self._normState(X)

        # Writes estimate to self variable
        self._X = X

        # Update state uncertanty covariance
        self._P = (np.eye(19)-K.dot(C)).dot(self._P)

    def _kalmanUpdate(self, X_m, C, R):
        # Function to call when new measurment is obtained

        # Computes residual Y
        Y = self._measure(X_m,C)

        # Computes kalman gain
        K = self._computeKalman(C,R)

        # Computes new state and uncertanty covariance matrix
        self._update(K,Y,C)

    def setMeasurment(self, msg):
        # Function to set measurment to Marker location

        # Resets counter
        self._estsSinceLastMeasure = 0

        # Parses data from msg
        rvec = msg.rvec
        tvec = msg.tvec
        self._id = msg.id

        # Calculates the quartonion from rvec
        quat = self._rodToQuat(rvec)

        # Creats measurment vector
        X_m = np.zeros((7,1))
        X_m[0] = tvec[0]
        X_m[1] = tvec[1]
        X_m[2] = tvec[2]
        X_m[3] = quat[0]
        X_m[4] = quat[1]
        X_m[5] = quat[2]
        X_m[6] = quat[3]

        # Gets parameters from self variables
        C = self._C
        R = self._R

        # Calls function to update filter
        self._kalmanUpdate(X_m, C, R)

    def getEstimationMarkMsg(self):
        # Function returns a aruco msg

        msg = self._arucoMsg

        # Populates the msg
        tvec = np.transpose(self._X[0:3])
        msg.tvec = tvec[0]
        
        quat = self._X[9:13]
        rod = self._quatToRod(quat)
        msg.rvec = rod

        msg.id = self._id

        return msg
