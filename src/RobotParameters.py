# File for storing robot data

import numpy as np

HRobCam = np.array([[-0.03, -0.33,  0.94, 0.04],
                    [-1.00,  0.00, -0.03, 0.01],
                    [ 0.01, -0.94, -0.33, 0.10],
                    [ 0.00,  0.00,  0.00, 1.00]])