#!/usr/bin/env python


# Importing librarys
import rospy
import numpy as np
import cv2
from std_msgs.msg import Int32
from geometry_msgs.msg import Twist
from mas507.msg import VelocityCommand
from mas507.msg import RobotPose2D
from mas507.msg import ArucoMarker
from mas507.msg import EmptyMsg

# Position from camera class
class PosFromCamera(object):

    def __init__(self):
        print('PosFromCam created!')
        
        # From robot to camera
        # From measured data
        self._HRobCam = np.array([  [-0.03, -0.19,  0.98, 0.06],
                                    [-1.00, -0.01, -0.03, 0.00],
                                    [ 0.02, -0.98, -0.18, 0.15],
                                    [ 0.00,  0.00,  0.00, 1.00]])
        
        # Matrixes for storing marker data, this is in essens the map of the world
        self._HmrkMat = np.zeros([4,4])

        # Marker 1
        rotMk1 = [0, np.pi/3]
        trnMk1 = np.array([[0.00],[1.35],[0.095]])
        rmat1 = self._rotZX(rotMk1)
        hmat1 = self._HmatFromMat(rmat1, trnMk1)
        self._HmrkMat = np.append(self._HmrkMat, hmat1, axis = 0)

        # Marker 2
        rotMk2 = [-np.pi/2, np.pi/3]
        trnMk2 = np.array([[1.00],[1.00],[0.095]])
        rmat2 = self._rotZX(rotMk2)
        hmat2 = self._HmatFromMat(rmat2, trnMk2)
        self._HmrkMat = np.append(self._HmrkMat, hmat2, axis = 0)

        # Marker 3
        rotMk3 = [np.pi, np.pi/3]
        trnMk3 = np.array([[0.60],[-0.10],[0.095]])
        rmat3 = self._rotZX(rotMk3)
        hmat3 = self._HmatFromMat(rmat3, trnMk3)
        self._HmrkMat = np.append(self._HmrkMat, hmat3, axis = 0)

        # Solution for an error not yet solved
        self._lastTheta = 0.0

    def posFromCam(self, msg):

        # Pars data form msg
        rvec = msg.rvec
        tvec = msg.tvec
        id = msg.id
        tvec = np.array([[tvec[0]],[tvec[1]],[tvec[2]]])

        # Finding HGlbMark based on id
        idxN = id*4

        HGlbMark = self._HmrkMat[idxN:idxN+4][0:4]

        # Get HCamMark from Rvec and Tvec
        HCamMark = self._HmatFromRvec(rvec, tvec)

        # Compute inverse of (HRobCam * HCamMark)^1, gives Hinv
        Hinv = np.linalg.inv(self._HRobCam.dot(HCamMark))

        # Compute Robot in world frame HGlbMark * Hinv = HGlbRobot
        HGlbRob = HGlbMark.dot(Hinv)

        # Find 2D pose from HGlobalRobot
        pose = self._poseFromHmat(HGlbRob)

        return pose

    def _poseFromHmat(self, Hmat):
        # Parsing data
        rmat = Hmat[0:3,0:3]
        tvec = Hmat[0:3,3]

        theta = self._thetaFromRmat(rmat)
        x = tvec[0]
        y = tvec[1]
        
        pose = np.array([[x],[y],[theta]])

        return pose

    def _thetaFromRmat(self, rmat):  

        # Added try, except to catch an unresolved error
        try:
            # Calculating lambda
            lam =rmat[0][0]*rmat[1][1]-rmat[0][1]*rmat[1][0]

            # Checking if lambda value is to small
            if np.abs(lam) < 0.1:
                # If you get  this error, something is very wrong
                lam = np.sign(lam)*0.1
                print('lam is very smal')

            # Calculating normelized vectors
            r1 = np.sqrt(1/np.abs(lam))*np.array([[rmat[0][0]],[rmat[1][0]],[0]])
            r2 = np.sqrt(1/np.abs(lam))*np.array([[rmat[0][1]],[rmat[1][1]],[0]])
            r3 = np.array([[0],[0],[1]])

            # Making a new rotational matrix
            rmatCor = np.append(r1, r2, axis = 1)
            rmatCor = np.append(rmatCor, r3, axis = 1)

            thetaEst1 = np.arctan2(rmatCor[1][0],rmatCor[0][0])
            thetaEst2 = np.arctan2(-rmatCor[0][1],rmatCor[1][1])
            thetaEst3 = np.arctan2(-rmatCor[0][1],rmatCor[0][0])
            thetaEst4 = np.arctan2(rmatCor[1][0],rmatCor[1][1])
            theta = (thetaEst1+thetaEst2+thetaEst3+thetaEst4)/4

            # Conditioning theta so it returns values between 0 and 2pi
            if(theta < 0):
                    theta = theta + 2*np.pi

            self._lastTheta = theta

            return theta

        except:
            print('PoseEkf: Theta from Rmat: error occured!, returned last good value')
            return self._lastTheta
    
    def _rotZX(self, rot):
        # Rotates first around marker x the marker z

        return self._Rz(rot[0]).dot(self._Rx(rot[1]))

    def _HmatFromMat(self, rmat, tvec):
        # Returns homogeneus matrix of rmat and tvec
        
        bottomRow = np.array([[0,0,0,1]])

        inter1 = np.append(rmat, tvec, axis = 1)
        Hom = np.append(inter1, bottomRow, axis = 0)

        return Hom

    def _HmatFromRvec(self, rvec, tvec):
        # Return homogeneus matrix from rotation vector 
        # and tvec using Rodrigues formulation

        rmat, jacobian = cv2.Rodrigues(rvec)

        Hom = self._HmatFromMat(rmat, tvec)

        return Hom

    def _Rx(self,phi):
        # Rotaing the object not the world frame
        return np.array([
            [1, 0       , 0       ],
            [0, np.cos(phi),-np.sin(phi)],
            [0, np.sin(phi), np.cos(phi)]
        ])

    def _Ry(self,theta):
        # Rotaing the object not the world frame
        return np.array([
            [ np.cos(theta), 0, np.sin(theta)],
            [ 0         , 1, 0         ],
            [-np.sin(theta), 0, np.cos(theta)]
        ])

    def _Rz(self,psi):
        # Rotaing the object not the world frame
        return np.array([
            [ np.cos(psi),-np.sin(psi), 0],
            [ np.sin(psi), np.cos(psi), 0],
            [0       , 0       , 1]
        ])

# Class to find angle error from setpoint and pose
class AngleError(object):

    def __init__(self):
        # Setpoint
        self._error = 0.0

        # Robot angle
        self._poseTheta = 0.0

        # Pose setpoint
        self._poseSetPoint = 0.0

    def setPredict(self, theta):
        # Function to set the angle of the robot in the world frame
        self._poseTheta = theta

    def setMeasure(self, theta):
        # Function to set pose setpoint

        # Checks that setpoint is in the range of [0, 2*pi]
        theta = np.remainder(theta, 2*np.pi)

        self._poseSetPoint = theta

    def _compute(self):
        # Function to compute setpoint
        SP = self._poseSetPoint
        PV = self._poseTheta

        e = 0

        # Computes the two possible solutions
        e1 = SP - PV

        if e1 < 0:
            e2 = 2*np.pi + e1
        else:
            e2 = e1 - 2*np.pi

        # Finds the shortest path
        if np.abs(e1) < np.abs(e2):
            e = e1
        else:
            e = e2

        self._error = e

    def getInovation(self):
        # Computes and returns the theta error

        self._compute()

        return self._error

# Robot Pose EKF class
class EkfPose(object):

    def __init__(self,dt):
        #Print init message
        print('EKF created')

        # Rate of predictions
        self._dt = dt

        # Creats class object to compute position from camera
        self._cam = PosFromCamera()

        # Creats class object to check for compleat and valid msg
        self.msgCompl = MsgCompl()

        # Class to calculate true inovation for theta, as 0 and 2*pi are next to each other
        self._thetaInovation = AngleError()

        # System matrix A and B are defined in functions

        # State of system and system uncertanty covariance matrix
        # [x_g, y_g, xDot_l, xDDot_l, xDDot_bias, t, tDot, tDDot, tDot_bias]
        self._X = np.zeros((9,1))
        self._X[0][0] = 0.05
        self._X[1][0] = 0.05
        self._X[5][0] = np.pi/2
        self._P = np.zeros((9,9))

        # Sudo state, this state is a pure integration of movments in the robots local frame
        # used for turn controller and for controller to drive forwards/backwards in local frame
        # [locX, locTheta]
        self._X_sudo = np.zeros((2,1), dtype = float)

        # Model and Measurment uncertanty covariance

        # Model
        # Uncertanty in numerical integrated states
        Q_n = 0.05
        # Uncertanty in change in linear accel from frame to frame         
        Q_acc_l = 0.5
        # Uncertanty in change in linear accel bias from frame to frame
        Q_acc_l_bias = 0.000001
        # Uncertanty in change in angular accel from frame to frame
        Q_acc_o = 5.0
        # Uncertanty in change in angular rate bias from frame to frame
        Q_o_bias = 0.000001

        # Defining vector to use in diagonal function
        Q_vec = [Q_n**2, Q_n**2, Q_n**2, Q_acc_l**2, Q_acc_l_bias**2, Q_n**2, Q_n**2, Q_acc_o**2, Q_o_bias**2]
        self._Q = np.diag(Q_vec)

        # Measurment uncertanty camera
        # Uncertanty in x and y dir
        self._v_cam_l = 0.4
        # Uncertanty in theta dir
        self._v_cam_o = 1.0

        # Measurment uncertanty imgur
        # Uncertanty in accel meassurment
        self._v_acc = 2.5
        # Uncertanty in omega meassurment
        self._v_omg = 0.001

        # Time constants for linear and angular accelerations
        self._tau_x = 0.05
        self._tau_o = 0.05

        # Calibration scaling for turn rate and drive rate
        self._velCalib = 0.9
        self._rotCalib = 0.0  # 2*np.pi/7.4

        # Threshold for cuting of theta updates from camera when rotation is to high
        self._omegaThetaMax = 1.0

        # Velocity comands
        self._velocityCmd = np.array([[0.0], [0.0]])

    def _A(self):
        # Calculating sin and cos of theta outside matrix
        theta = self._X[5,0]
        c = np.cos(theta)
        s = np.sin(theta)

        # short notation for dt
        dt = self._dt

        # time constants for linear and angular accelerations
        tx = 1.0/self._tau_x
        to = 1.0/self._tau_o

        # Defining A matrix
        A = np.array([  [1, 0,c*dt,  0,  0,  0,  0,  0,  0],
                        [0, 1,s*dt,  0,  0,  0,  0,  0,  0],
                        [0, 0,   1, dt,  0,  0,  0,  0,  0],
                        [0, 0, -tx,  0,  0,  0,  0,  0,  0],
                        [0, 0,   0,  0,  1,  0,  0,  0,  0],
                        [0, 0,   0,  0,  0,  1, dt,  0,  0],
                        [0, 0,   0,  0,  0,  0,  1, dt,  0],
                        [0, 0,   0,  0,  0,  0,-to,  0,  0],
                        [0, 0,   0,  0,  0,  0,  0,  0,  1],])

        return A

    def _B(self):
        # Function for B matrix ,Calculating sin and cos of theta outside matrix

        # time constants for linear and angular accelerations
        tx = 1.0/self._tau_x
        to = 1.0/self._tau_o

        # Creating B matrix
        B = np.array([  [0, 0],
                        [0, 0],
                        [0, 0],
                        [tx,0],
                        [0, 0],
                        [0, 0],
                        [0, 0],
                        [0,to],
                        [0, 0],])

        return B

    def predict(self):
        # Function for predicting where the robot is located
        # Predicting where the robot is by unlinear function
        X_p = self._A().dot(self._X) + self._B().dot(self._velocityCmd)

        # Updating uncertanty covariance matrix
        P_p = self._A().dot(self._P.dot(np.transpose(self._A()))) + self._Q

        # Wraps predisction value
        X_p[5][0] = np.remainder(X_p[5][0], 2*np.pi)

        # Setting predicted values to the output values
        self._X = X_p
        self._P = P_p

    def sudoStateIntegrate(self):
        # predicts/integrates sudo state bases on estimates
        
        # Parses self variables
        X_sudo = self._X_sudo
        dt = self._dt

        # Sets up vector with linear and angular velocity
        dX_sudo = np.array([[self._X[2][0]],[self._X[6][0]]])

        X_sudo = X_sudo + dt*dX_sudo

        self._X_sudo = X_sudo

    def _measure(self, X_measure, C):
        # Finds the error between the measurment and the predicted value


        if C.shape[0] == 1:
            self._thetaInovation.setMeasure(X_measure)
            self._thetaInovation.setPredict(C.dot(self._X))
            inovation = self._thetaInovation.getInovation()
        else:
            inovation = X_measure - C.dot(self._X)


        return inovation

    def _computeKalman(self,C,R):
        # Computes kalman gain

        S = C.dot(self._P.dot(np.transpose(C))) + R

        return self._P.dot(np.transpose(C).dot(np.linalg.inv(S)))

    def _update(self,K,Y,C):
        # Updates estimate
        self._X = self._X + K.dot(Y)
        self._P = (np.eye(9)-K.dot(C)).dot(self._P)

    def _kalmanUpdate(self, X_m, C, R):
        # Function to call when new measurment is obtained

        # Computes residual Y
        Y = self._measure(X_m,C)

        # Computes kalman gain
        K = self._computeKalman(C,R)

        # Computes new state and uncertanty covariance matrix
        self._update(K,Y,C)

    def getEstimateState(self):
        # Function to get estimated state
        return self._X

    def getEstimatePoseMsg(self):
        # Function to get estimated position
        xPos = self._X[0]
        yPos = self._X[1]
        thetaPos = self._X[5]

        # Function returns RobotPose msg
        pose = RobotPose2D()
        pose.x = xPos
        pose.y = yPos
        pose.theta = thetaPos

        return pose

    def getEstimateVelMsg(self):
        # Function to get estimated position
        xVel = self._X[2]
        xAcc = self._X[3]
        thetaVel = self._X[6]

        # Function returns RobotPose msg
        vel = RobotPose2D()
        vel.x = xVel
        vel.y = xAcc
        vel.theta = thetaVel

        return vel

    def getEstimateLocPoseMsg(self):
        # Function to get local pose state

        xPosLoc = self._X_sudo[0][0]
        thetaPosLoc = self._X_sudo[1][0]

        # Function returns RobotPose msg
        poseLoc = RobotPose2D()
        poseLoc.x = xPosLoc
        poseLoc.theta = thetaPosLoc

        return poseLoc

    def resetLocPosEstimate(self, msg):
        # Function to reset estimate of sudo state
        self._X_sudo = np.zeros((2,1), dtype = float)

    def getEstimateCovariance(self):
        # Function to return estimate uncertanty covariance
        return self._P

    def setVelocityCMD(self, msg):
        # Function to set velocity commands to class members
        # Parsing msg data
        u_v = msg.xDot
        u_o = msg.thetaDot

        # Calibration constants
        vC = self._velCalib
        rC = self._rotCalib

        # Friction offsets
        u_v_offset = 0.06
        u_o_offset = 1.50

        # To low of a command signal dose not actuates the motors
        if np.abs(u_v) < u_v_offset:
            u_v = 0
        
        if np.abs(u_o) < u_o_offset:
            u_o = 0
        else:
            u_o = u_o -np.sign(u_o)*u_o_offset

        #Setting parsed data to self variabel
        self._velocityCmd = np.array([[u_v*vC], [u_o*rC]])

    def setCamMeasurment(self, msgIn):
        # Function to set the measured camera input, data from camera is also filtered trough EQKF filter
        
        # Checks for msg validaty
        msgChecked = self.msgCompl.checkMsg(msgIn)

        # Checks for msg validaty
        msgChecked = self.msgCompl.checkMsg(msgIn)
        
        # Calls member function from camera object to compute the position based on the aruco msg
        X_measure = self._cam.posFromCam(msgChecked)

        ## Updates in two steps, first position, then theta, if yaw rate is low ##

        ### Position update ###
        X_pos = X_measure[0:2]

        # C matrix for camera position
        C_pos = np.array([  [1,0,0,0,0,0,0,0,0],
                            [0,1,0,0,0,0,0,0,0]])

        # Measurment position uncertanty
        v_cam_l = self._v_cam_l

        R_pos = np.diag([v_cam_l**2, v_cam_l**2])

        if np.abs(self._velocityCmd[1][0]) < self._omegaThetaMax:
        
            # Calls function to update the position based on camera input
            self._kalmanUpdate(X_pos,C_pos,R_pos)

        else:
            # Setting uncertanty covariance higher when camera is ignored
            self._P[0][0] = 10.0*v_cam_l
            self._P[1][1] = 10.0*v_cam_l
            self._P[3][3] = 1.0

            self._P[0][1] = 10.0*v_cam_l
            self._P[1][0] = 10.0*v_cam_l

        ### Theta update ###
        X_theta = X_measure[2]

        # C matrix for camera theta
        C_theta = np.array([[0,0,0,0,0,1,0,0,0]])

        # Uncertanty in theta dir
        v_cam_o = self._v_cam_o

        R_theta = np.diag([v_cam_o**2])

        if np.abs(self._velocityCmd[1][0]) < self._omegaThetaMax:

            # Calls function to update the position based on camera input
            self._kalmanUpdate(X_theta,C_theta,R_theta)

        else:
            print('Theta ignored !')
            # Setting uncertanty covariance higher when camera is ignored
            self._P[5][5] = 10.0*v_cam_o

    def setImuMeasurment(self, msg):
        # Function to set the measured acc input

        # Parse data and set it to the self variable
        # Linear acceleration in x-dir in robot local frame
        accX = msg.linear.x
        # Angular rate around z-dir in robot local frame
        omegaZ = -msg.angular.z

        # First time the function is called the acc and gyro measurment is set as bias
        if self._X[4] == 0.0:
            self._X[4] = accX
            self._X[8] = omegaZ

        # Defines measurment vector
        X_measure = np.array([[accX],[omegaZ]])

        # C matrix for IMU
        C = np.array([  [0,0,0,1,1,0,0,0,0],
                        [0,0,0,0,0,0,1,0,1]])

        # Measurment uncertanty
        # Uncertanty in accel meassurment
        v_acc = self._v_acc
        # Uncertanty in omega meassurment
        v_omg = self._v_omg

        R = np.diag([v_acc**2, v_omg**2])

        # Calls function to update the position based on camera input
        self._kalmanUpdate(X_measure,C,R)

# Message completness class
class MsgCompl(object):
    # Object to check that a ArucoMarker.msg is compleat and valid

    def __init__(self):
        # Creats initial emepty msg
        msg_0 = ArucoMarker()
        msg_0.tvec = np.zeros(3)
        msg_0.rvec = np.zeros(3)
        msg_0.id = 0

        # Variable to stor last valid msg
        self._lastValidMsg = msg_0

    def checkMsg(self, msg):

        if self._msgTest(msg) == 1:
            msgReturn = msg

        else:
            msgReturn = self._lastValidMsg
            print('Msg failed')

        return msgReturn

    def _msgTest(self, msg):
        # Returns 1 of msg is valid, returns 0 if msg is false
        status = 1

        # checks size of vectors
        if len(msg.tvec) != 3:
            status = 0
        if len(msg.rvec) != 3:
            status = 0

        return status


if __name__ == '__main__':
    # initialize node
    rospy.init_node('PoseEKF')

    try:

        # Declares rate
        rate = rospy.get_param('~rate', 50.0)
        dt = 1.0/rate

        # Creates pose EKF object
        poseEkf = EkfPose(dt)

        # Define subscribers
        cmdVelocitySubscriber = rospy.Subscriber('sens/VelSentToWheel', VelocityCommand, poseEkf.setVelocityCMD)
        cameraSubscriber = rospy.Subscriber('sens/measurment/cam', ArucoMarker, poseEkf.setCamMeasurment)
        imuSubscriber = rospy.Subscriber('sens/measurment/imu', Twist, poseEkf.setImuMeasurment)
        resetLocPoseSubscriber = rospy.Subscriber('ekf/reset/LocPose',EmptyMsg ,poseEkf.resetLocPosEstimate)

        # Define publisher
        positionPublisher = rospy.Publisher('ekf/state/pose', RobotPose2D, queue_size=1)
        poseEstimate = RobotPose2D()

        locPositionPublisher = rospy.Publisher('ekf/state/locPose', RobotPose2D, queue_size=1)
        locPoseEstimate = RobotPose2D()

        velocityPublisher = rospy.Publisher('ekf/state/vel', RobotPose2D, queue_size=1)
        velEstimate = RobotPose2D()
        
        
         # Keep node alive
        execRate = rospy.Rate(rate)
        while not rospy.is_shutdown():
            
            # Predicts position
            poseEkf.predict()

            # Updates position in local frame
            poseEkf.sudoStateIntegrate()

            # Gets estimate from EKF and Publishes
            poseEstimate = poseEkf.getEstimatePoseMsg()
            velEstimate = poseEkf.getEstimateVelMsg()
            locPoseEstimate = poseEkf.getEstimateLocPoseMsg()

            positionPublisher.publish(poseEstimate)
            velocityPublisher.publish(velEstimate)
            locPositionPublisher.publish(locPoseEstimate)


            # Sleeps the remeining time
            execRate.sleep()

    except rospy.ROSInterruptException:
        pass

