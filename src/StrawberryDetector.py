import cv2
from cv_bridge import CvBridge
import numpy as np
from mas507.msg import Polar, Detection

# Inspired from: https://github.com/andridns/cv-strawberry
class StrawberryDetector(object):
    def __init__(self, image_publisher, polar_publisher, detection_publisher, mtx, dist):
        self.cvBridge = CvBridge()
        self.ros = image_publisher
        self.polarPub = polar_publisher
        self.detectionPub = detection_publisher
        self.mtx = mtx
        self.dist = dist
        self._strawBerryDistanceMaxThreshold = 0.375
        self._strawBerryDistanceMinThreshold = 0.12
        self._strawBerryAngleThreshold = 60.0/2.0*np.pi/180.0
        self._stbAngleOffset = 0.0
        self.x = 0
        self.y = 0
        self.z = 0
        self._HRobCam = np.array([  [-0.03, -0.19,  0.98, 0.06],
                                    [-1.00, -0.01, -0.03, 0.00],
                                    [ 0.02, -0.98, -0.18, 0.15],
                                    [ 0.00,  0.00,  0.00, 1.00]])

        # Define messages
        self.detectionMsg = Detection()
        self.polarMsg = Polar()


    def detect(self, image):
        # Read image message and convert to CV image
        image = image

        # Convert from BGR to RGB
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

        # Blur image slightly
        image_blur = cv2.GaussianBlur(image, (7, 7), 0)

        # Convert to HSV
        image_blur_hsv = cv2.cvtColor(image_blur, cv2.COLOR_RGB2HSV)

        # 0-8 hue
        min_red = np.array([0, 100, 105])
        max_red = np.array([5, 255, 255])
        image_red1 = cv2.inRange(image_blur_hsv, min_red, max_red)

        # 171-179 hue
        min_red2 = np.array([175, 100, 105])
        max_red2 = np.array([179, 256, 256])
        image_red2 = cv2.inRange(image_blur_hsv, min_red2, max_red2)

        # Create red image
        image_red = image_red1 + image_red2

        # Fill small gaps and remove specks
        kernel = cv2.getStructuringElement(cv2.MORPH_ELLIPSE, (15, 15))
        image_red_closed = cv2.morphologyEx(image_red, cv2.MORPH_CLOSE, kernel)
        image_red_closed_then_opened = cv2.morphologyEx(image_red_closed, cv2.MORPH_OPEN, kernel)

        # Define standard value for detection parameters
        strawBerryDetection = 0
        strawBerryAngle = 0
        strawBerryDistance = 0

        # Find biggest red countour
        big_contour, red_mask = self.find_biggest_contour(image_red_closed_then_opened)

        if (big_contour is not None) and (red_mask is not None):
            # Centre of mass
            moments = cv2.moments(red_mask)
            centre_of_mass = int(moments['m10'] / moments['m00']), int(moments['m01'] / moments['m00'])

            # Add point of COM in image
            cv2.circle(image, centre_of_mass, 5, (0, 255, 0), -1, cv2.LINE_AA)

            # Fit ellipse to detection
            ellipse = cv2.fitEllipse(big_contour)
            cv2.ellipse(image, ellipse, (0, 255, 0), 1)

            # Strawberry radius based on ellipse fitting
            r = int(ellipse[1][0]/2)

            # Add circle to image around COM and image boundary using calculated radius
            cv2.circle(image, centre_of_mass, r, (0, 0, 255), 1, cv2.LINE_AA)
            
            # PnP for strawberry location
            n = 30
            t = np.linspace(0, 2*np.pi, n)

            # 3D points of strawberry major circkle
            radius_strawberry = (32.0/1000.0)/2
            objpnts = np.zeros([n,3])
            objpnts[:,0] = radius_strawberry*np.sin(t)
            objpnts[:,1] = radius_strawberry*np.cos(t)

            # 2D points in image of detected strawberry major circle
            imgpoints = np.zeros([n,2])
            imgpoints[:,0] = centre_of_mass[0] + r*np.sin(t)
            imgpoints[:,1] = centre_of_mass[1] + r*np.cos(t)
            
            # Solve for position and orientation
            _, rvec, tvec = cv2.solvePnP(objpnts, imgpoints, self.mtx, self.dist)
            
            # Return detection results to class
            HCamStrawberry = self._HmatFromRvec(rvec, tvec)

            HRobStrawberry = self._HRobCam.dot(HCamStrawberry)


            # Get coordinate of strawberry
            self.x = HRobStrawberry[0,3]
            self.y = HRobStrawberry[1,3]
            self.z = HRobStrawberry[2,3]


            # Find angle and distance to strawberry in the plane.
            strawBerryAngle = np.arctan2(self.y, self.x)
            strawBerryDistance = np.sqrt((self.x)**2 + (self.y)**2)

            #print(np.abs(strawBerryAngle) < self._strawBerryAngleThreshold)



            # If detected contour is closer than the threshold, set detection to 1
            strbryMaxR = self._strawBerryDistanceMaxThreshold
            strbryMinR = self._strawBerryDistanceMinThreshold
            strbryAngThr = self._strawBerryAngleThreshold

            check = (strawBerryDistance < strbryMaxR) and (strawBerryDistance > strbryMinR) and (np.abs(strawBerryAngle) < strbryAngThr)

            if check == 1:
                strawBerryDetection = 1

            self.detectionMsg.data = strawBerryDetection
            self.polarMsg.radius = strawBerryDistance
            self.polarMsg.angle = strawBerryAngle - self._stbAngleOffset



        # Publish messages
        image_bgr = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
        self.ros.publish(self.cvBridge.cv2_to_imgmsg(image_bgr, "bgr8"))
        self.detectionPub.publish(self.detectionMsg)

        if strawBerryDetection == 1:
            self.polarPub.publish(self.polarMsg)


        
        
    def _HmatFromRvec(self, rvec, tvec):
        # Return homogeneus matrix from rotation vector 
        # and tvec using Rodrigues formulation

        rmat, jacobian = cv2.Rodrigues(rvec)

        bottomRow = np.array([[0,0,0,1]])

        inter1 = np.append(rmat, tvec, axis = 1)
        Hom = np.append(inter1, bottomRow, axis = 0)

        return Hom

    def find_biggest_contour(self, image):
        # Copy to prevent modification
        image = image.copy()
        contours, hierarchy = cv2.findContours(image, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

        # Isolate largest contour
        contour_sizes = [(cv2.contourArea(contour), contour) for contour in contours]

        # Return results
        if len(contour_sizes) != 0:
            biggest_contour = max(contour_sizes, key=lambda x: x[0])[1]
    
            mask = np.zeros(image.shape, np.uint8)
            cv2.drawContours(mask, [biggest_contour], -1, 255, -1)

        else:
            biggest_contour = None
            mask = None

        return biggest_contour, mask







