#!/usr/bin/env python
"""
Node for controlling Jetbot robot
"""

import rospy
import numpy as np
from std_msgs.msg import Int64
from sensor_msgs.msg import Joy
from std_msgs.msg import Int32
from mas507.msg import VelocityCommand, ServoSetpoints, ArmCommand
import Adafruit_PCA9685


# Driving (wheels)
class DiffDrive(object):
    def __init__(self):
        # Define centerpoint for velocity and zero point for arm angle
        self._motorOffsetTic = np.array([[4.0], [4.0]])
        self._motorMinCmd = 261
        self._motorMaxCmd = 351

        self._motorWheelBase = rospy.get_param('~DiffDrive/motorWheelBase', 0.117)
        self._motorMaxVel = rospy.get_param('~DiffDrive/motorMaxVelocity', 0.366)

        # Define publisher
        self._pubCmdSent = rospy.Publisher('sens/VelSentToWheel', VelocityCommand, queue_size=1)

        # Define messages
        self._msgCmdSent = VelocityCommand()

        # Initial values
        self._rightWheel = 310
        self._leftWheel = 310

        # Contants
        b = self._motorWheelBase
        self._aMat = np.array([[-0.5, 0.5],[-1.0/b, -1.0/b]])   
        


    def publishEKFMessage(self):
        # Publish message
        self._pubCmdSent.publish(self._msgCmdSent)

    # Setter
    def setVelocities(self, velocitySP, turnRateSP):
        
        # Create setpoints for different motors
        cmdGlobalFrame = np.array([[velocitySP], [turnRateSP]])

        # Converting comand signal from robot frame to wheel velocities
        wheelVelCmd = self._cmdToWheelVel(cmdGlobalFrame)

        # Converting command signal (m/s) to tics
        ticCmd = self._velCmdToTickCmd(wheelVelCmd, self._motorOffsetTic)

        # Saturating output to valid tic range
        motorSetpoints = np.clip(ticCmd, self._motorMinCmd, self._motorMaxCmd)
        
        # Round off to minimize error due to casting
        motorSetpoints = np.round(motorSetpoints,0)

        # Set wheel velocities in ticks
        self._rightWheel = motorSetpoints[0][0]
        self._leftWheel = motorSetpoints[1][0]

        # Calculating what was sent to the motors, this is used for odeometry localization
        # The reason for the back calculation is that what is commanded is not always what
        # is sent to the motor due to the saturation (np.clip) function
        wheelVelSent = self._ticCmdToVelCmd(motorSetpoints, self._motorOffsetTic)
        
        # Calculating the commands sent to the robot
        robotCmdSent = self._wheelVelToCmd(wheelVelSent)
        

        # Populate and publish message to EKF
        self._msgCmdSent.xDot = robotCmdSent[0]
        self._msgCmdSent.thetaDot = robotCmdSent[1]

    # Getter
    def getMotorSpeedTics(self):
        # Returns tic speed of right and left wheel
        return self._rightWheel, self._leftWheel


    def _velCmdToTickCmd(self, velCmd, ticOffset):
        numDiff = self._motorMaxCmd-self._motorMinCmd
        denDiff = 2*self._motorMaxVel
        cmdCenter = (self._motorMaxCmd-self._motorMinCmd)/2 + self._motorMinCmd

        tic = numDiff/denDiff * velCmd + (cmdCenter + ticOffset)

        return tic


    def _cmdToWheelVel(self, velCmd):
        # Find vel [Vr, Vl] from velcmd
        vel = np.linalg.inv(self._aMat).dot(velCmd)

        return vel
     

    def _ticCmdToVelCmd(self, ticCmd, ticOffset):
        numDiff = self._motorMaxCmd-self._motorMinCmd
        denDiff = 2*self._motorMaxVel
        cmdCenter = (self._motorMaxCmd-self._motorMinCmd)/2 + self._motorMinCmd

        return (ticCmd - (cmdCenter + ticOffset))/(numDiff/denDiff)


    def _wheelVelToCmd(self, wheelVel):
        # Define constants from self vairables
        cmd = self._aMat.dot(wheelVel)
        return cmd

# Scooping
class TheScoop(object):
    def __init__(self):
        # Assuming the full stroke of the servo (0-180 deg) is bound in the region (204-816 ticks)
        self._angMinCmd = 90.0
        self._angMaxCmd = 510.0
        self._rightServoAngZero = 0.0
        self._leftServoOffsetTic = 635
        self._actuationSpan = 40.0

        self._rightServoTic = self._angCmdToTickCmd(1.0, self._rightServoAngZero, 180.0)
        self._leftServoTic = self._leftServoOffsetTic - self._rightServoTic

    # Setter
    def setScoopCommand(self, servoSP):
        # Sets Actuator setpoints
        angD = self._actuationSpan
        angCmd = angD/2 * (1.0 - servoSP) # Makes it so full press of trigger gives range of angD [1 -> -1 gives full range]
        rightServoTic = self._angCmdToTickCmd(angCmd, self._rightServoAngZero, 180.0)
        leftServoTic =  self._leftServoOffsetTic - rightServoTic

        self._rightServoTic = rightServoTic
        self._leftServoTic = leftServoTic

        #print('[Right, Left]')
        #print([rightServoTic, leftServoTic])
    
    # Getter
    def getServoTics(self):
        return self._rightServoTic, self._leftServoTic

    def _angCmdToTickCmd(self, angCmd, angBtm, degSpan):
        # Function to get tics from desired angle calculated from a user-defined bottom angBtm
        numDiff = (self._angMaxCmd - self._angMinCmd)
        denDiff = degSpan
        ticPerDeg = numDiff/denDiff

        tic = ticPerDeg * (angCmd + angBtm) + (self._angMinCmd)

        return tic

# CommandSignalListener class
class CommandSignalListener(object):

    def __init__(self):
        # Print Init message
        print('JetbotController: CommandSignalListener Created!')

        # Define subscribers
        self._velSub = rospy.Subscriber('controller/velocityCommand', VelocityCommand, self._velocityCallback)
        self._armSub = rospy.Subscriber('armCommand', ArmCommand, self._armCallback)
        self._joySub = rospy.Subscriber('joy', Joy, self._joyCallback)


        # Get launch parameters for ros node
        # Speed gain in m/s turn gain in rad/s
        self._gamepadSpeedGain = rospy.get_param('~CommandSignalListener/gamepadSpeedGain', 0.2)
        self._gamepadTurnGain = rospy.get_param('~CommandSignalListener/gamepadTurnGain', 2.0)

        # Set initial values of setpoints.
        self._initSetpoints()
        
        # Autonomous control bit
        self._autonomousEnable = 0

    def __del__(self):
        # Destructor of CommandSignalListener
        # Set velocities to 0
        self._initSetpoints()
        print('Deleting CommandSignalListener... Setting velocities to 0')

    def _initSetpoints(self):
        # Initializes setpoints to default values
        self._autoVelocitySP = 0.0
        self._autoTurnrateSP = 0.0
        self._autoServo1 = 1.0
        self._autoServo2 = 1.0
        self._autoServo3 = 1.0
        self._gamepadVelocitySP = 0.0
        self._gamepadTurnrateSP = 0.0
        self._gamepadServo1 = 1.0
        self._gamepadServo2 = 1.0
        self._gamepadServo3 = 1.0

    # Callbacks
    def _velocityCallback(self, msg):
        # Fill autonomous-SPs with data from message
        self._autoVelocitySP = msg.xDot
        self._autoTurnrateSP = msg.thetaDot

    def _armCallback(self, msg):
        # Get angle SPs from message [not angle, atm it's just 1 or -1]
        self._autoServo1 = msg.phi1
        self._autoServo2 = msg.phi2
        self._autoServo3 = msg.phi3

    def _joyCallback(self, msg):
        # Recieve and parse data
        btns = msg.buttons
        data = msg.axes
        controllerVelRaw = data[1]
        controllerTurnRaw = data[3]
        actuatorSP = data[5]
        startBtn = btns[7]
        stopBtn = btns[6]

        self._gamepadVelocitySP = controllerVelRaw*self._gamepadSpeedGain
        self._gamepadTurnrateSP = controllerTurnRaw*self._gamepadTurnGain
        self._gamepadServo1 = actuatorSP

        # Enable autonomous navigation and reset velocity
        if startBtn == 1:
            # Reset setpoints at mode change
            self._initSetpoints()
            self._autonomousEnable = 1

        if stopBtn == 1:
            # Reset setpoints at mode change
            self._initSetpoints()
            self._autonomousEnable = 0

    # Getters
    def getVelocities(self):
        # Function to set velocities
        velocitySP = 0
        turnRateSP = 0

        # Control by message 
        if self._autonomousEnable == 1:
            velocitySP = self._autoVelocitySP
            turnRateSP = self._autoTurnrateSP

        # Gamepad control
        if self._autonomousEnable == 0:
            velocitySP = self._gamepadVelocitySP
            turnRateSP = self._gamepadTurnrateSP

        return velocitySP, turnRateSP

    def getActuatorSetPoints(self):
        servo1SP = 1.0
        servo2SP = 1.0
        servo3SP = 1.0

        # Control by message 
        if self._autonomousEnable == 1:
            servo1SP = self._autoServo1
            servo2SP = self._autoServo2
            servo3SP = self._autoServo3

        # Gamepad control
        if self._autonomousEnable == 0:
            servo1SP = self._gamepadServo1
            servo2SP = self._gamepadServo2
            servo3SP = self._gamepadServo3

        return servo1SP, servo2SP, servo3SP

# JetbotController class
class JetbotController(object):
    def __init__(self):
        print('JetbotController: JetbotController created!')
        # Adafruit PCA board instance setup
        self.__StandStillMessage = ServoSetpoints()
        self.adafruit = Adafruit_PCA9685.PCA9685(busnum=1)
        self.adafruit.set_pwm_freq(50)

        
    def __del__(self):
        self.__StandStillMessage.rightWheel = 310
        self.__StandStillMessage.leftWheel = 310
        self.setPwm(self.__StandStillMessage)
        print('Stopping motors... deleting JetbotController')

    def setPwm(self, setPoints):
        """
        Set RC parameter for wheel motors driven by Adafruit
        PCA9685 with 12 bit resolution (2^12 = 4096) for each servo
        On 50 Hz rate (20 ms period), 409 ticks=2ms, 204ticks=1ms

        Parameters
        ----------
        u[i]:
            Uptime of RC signal in ticks
            Between [204 and 408]
            where, theoretically, 306 is stand-still, 204 is maximum reverse
            and 408 is maximum forward.

            i=0: right motor
            i=1: left motor
            i=2: servo 1
            i=3: servo 2
            i=4: servo 3
        """
        # Save last incoming message
        self.__StandStillMessage = setPoints

        u = [
            int(setPoints.rightWheel),
            int(setPoints.leftWheel),
            int(setPoints.servo1),
            int(setPoints.servo2),
            int(setPoints.servo3)
        ]
        for i in range(0, 5):
            # Limit input to u in [204, 408]
            if i < 2:
                # Upper limit for wheel motors
                # Upper RC protocol limit
                u[i] = max(204, u[i])
                u[i] = min(408, u[i])

            else:
                # Upper limit for arm motors
                u[i] = max(90,u[i])
                u[i] = min(816, u[i])
            
            self.adafruit.set_pwm(i, 0, u[i])

# Low pass filter
class LowPass(object):
    # Low pass filter, compute function must be ran every dt time step

    def __init__(self, dt, tau = 0.5, init = 0.0):
        self._dt = dt
        self._y = init
        self._y_1 = init
        self._alpha = tau/(tau + dt)
        self._u = init

    def compute(self):
        # sets the input, computes new value and returns the filter value
        
        # gets aplha from self variables
        a = self._alpha

        # Computes new filter value
        self._y = (1-a)*self._u + a* self._y_1

        # Sets new value to old value
        self._y_1 = self._y

    def reset(self, init = 0.0):
        # Dose what it says, resets the value of the filter, can also give value to set it to

        self._y_1 = init

    def setTau(self, tau):
        # Sets new filter constant
        self._alpha = tau/(tau + self._dt)

    def setTarget(self, u):
        # function to set target
        self._u = u

    def getOutput(self):
        # Function to get filter output
        return self._y

# Ramp class
class Ramp(object):
    # Ramps the setpoint

    def __init__(self, dt , slope = 1.0, init = 0.0):
        self._slope = slope
        self._dt = dt
        self._output = init
        self._target = init
        self._e = slope*dt

    def compute(self):
        # ramps output

        # Shorter notation 
        out = self._output
        trg = self._target
        slp = self._slope
        dt = self._dt
        e = self._e

        if (out > trg-e) and (out < trg+e):
            # if we are in range of target, out is target
            out = trg
        else:
            # Else ranp up or down
            if out < trg:
                out = out + slp*dt
            elif out > trg:
                out = out - slp*dt
        
        # Sets short variables to self variables
        self._output = out

    def getOutput(self):
        # function to get current ramp value
        return self._output

    def setTarget(self,target):
        # sets target
        self._target = target

    def reset(self):
        #Sets output value to the target value
        self._output = self._target

# Filtered ramp
class FilteredRamp(object):
    # Filtered ramp, is a ramp that is low passed filtered

    # This class sets the main interface, to set slope and tau use sub classes interfaces

    def __init__(self, dt, tau, slope, init):
        self._dt = dt
        self._tau = tau
        self._slope = slope
        self._init = init

        # Defines classes
        self._lpf = LowPass(self._dt, self._tau, self._init)
        self._ramp = Ramp(self._dt, self._slope, self._init)

    def compute(self):

        # Updates ramp function
        self._ramp.compute()
        # Gets ramp value
        rmpVal = self._ramp.getOutput()

        # Sets ramp value to lpf setpoint
        self._lpf.setTarget(rmpVal)
        # Updates lpf
        self._lpf.compute()

    def setTarget(self, target):
        # Sets target to ramp function
        self._ramp.setTarget(target)

    def getOutput(self):
        # Gets output from lpf
        return self._lpf.getOutput()

    def reset(self):
        # resets ramp and lp filter
        self._ramp.reset()
        self._lpf.reset()


if __name__ == '__main__':
    try:
        # Init ROS node
        rospy.init_node('jetbotController')
        executionRate = rospy.get_param('~/rate', 100.0)

        # Setup controllers
        jetbotController = JetbotController()
        comSigLis = CommandSignalListener()
        diffDrive = DiffDrive()
        scoop = TheScoop()

        servoSetpoints = ServoSetpoints()

        # Get initial values from scoop and diff.drive
        servoSetpoints.servo1, servoSetpoints.servo2 = scoop.getServoTics()

        # Filter params
        filterDt = 1.0/executionRate
        motorTau = 0.01
        motorRamp = 10
        actuatorTau = 0.01
        actuatorRamp = 175.0

        # Initialize filters
        velSPFilter = FilteredRamp(filterDt, motorTau, motorRamp, 0)
        turnRateFilter = FilteredRamp(filterDt, motorTau/12, motorRamp*12.0, 0)
        rightServoFilter = FilteredRamp(filterDt, actuatorTau, actuatorRamp, servoSetpoints.servo1)
        leftServoFilter = FilteredRamp(filterDt, actuatorTau, actuatorRamp, servoSetpoints.servo2)
    
        # Set loop rate from CommandSignalListener
        rate = rospy.Rate(executionRate)
        while not rospy.is_shutdown():
            # Get velocity & SCOOP setpoints from listener
            velSP, turnRateSP = comSigLis.getVelocities()
            servo1SP, servo2SP, servo3SP = comSigLis.getActuatorSetPoints()


            ###### Call vel & scoop setters ######
            # Filter then set velocities to diff drive & publish message
            velSPFilter.setTarget(velSP)
            velSPFilter.compute()
            turnRateFilter.setTarget(turnRateSP)
            turnRateFilter.compute()

            velSP = velSPFilter.getOutput()
            turnSP = turnRateFilter.getOutput()

            diffDrive.setVelocities(velSP, turnSP)
            diffDrive.publishEKFMessage()

            # Set SCOOP commands
            scoop.setScoopCommand(servo1SP)


            ###### Call vel & scoop getters ######
            # Get wheel command tics from diffDrive
            rightWheelTic, leftWheelTic = diffDrive.getMotorSpeedTics()

            # Get SCOOP tic commands
            servo1Tic, servo2Tic = scoop.getServoTics()


            # Filter servo tic commands
            rightServoFilter.setTarget(servo1Tic)
            leftServoFilter.setTarget(servo2Tic)
            rightServoFilter.compute()
            leftServoFilter.compute()

            ###### Collect gotten tic values and set pwm output ######
            # Collect setpoints in struct
            servoSetpoints.rightWheel = rightWheelTic
            servoSetpoints.leftWheel = leftWheelTic
            servoSetpoints.servo1 = rightServoFilter.getOutput()
            servoSetpoints.servo2 = leftServoFilter.getOutput()
            servoSetpoints.servo3 = 204

            # Set pwm to wheels
            jetbotController.setPwm(servoSetpoints)

            # Sleep remainder
            rate.sleep()

    except rospy.ROSInterruptException:
        pass
