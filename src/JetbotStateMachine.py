#!/usr/bin/env python


# Importing librarys
import rospy
import numpy as np
import time
from sensor_msgs.msg import Joy
from mas507.msg import DriverSetpoint, EmptyMsg, Detection, ArmCommand


class StateMachine(object):

    def __init__(self, setPointPub, ekfResetPub, scoopPub):
        # Number of bushes and distance between "lanes" of travel
        self._nBush = 5
        self._firstLanePos = -0.075
        self._secoundLanePos = 0.725
        self._interPlantDist = 0.16

        # Publishers
        self._setPointPub = setPointPub
        self._resetEkfLocPub = ekfResetPub
        self._scoopPub = scoopPub
        
        # Target modes:    0 - Do nothing
        #                   1 - Pure Pursuit
        #                   2 - Angle controller
        #                   3 - Drive towards berry
        #                   4 - Forward/back in local frame
        self._targetIdleMode = 0
        self._targetMoveMode = 1
        self._targetTurnMode = 2
        self._targetBerryMode = 3
        self._targetReturnMode = 4

        # Vector of positions
        SPXVecFirstLane = self._firstLanePos*np.ones(self._nBush)
        SPXVecSecLane = self._secoundLanePos*np.ones(self._nBush)
        SPX = np.append(SPXVecFirstLane, SPXVecSecLane)
        SPY = np.zeros(2*self._nBush)
        for i in range(self._nBush):
            SPY[(len(SPY)-1)-i] = (i)*self._interPlantDist + 0.25
            SPY[i] = (i)*self._interPlantDist + 0.25
        self._SPVec = np.transpose(np.array([[SPX], [SPY]]))

        # Print for debugging
        print(self._SPVec)
        
        # Ready bit to signal Driver ready for new SP
        self.targetReady = 1

        # Berry detection
        self._berryDetected = 0
        self._betterDetectionWaitTimer = 1.5

        # Scooping
        self._scoopTime = 1.5

        # Initiate message
        self._setPointMsg = DriverSetpoint()
        self._resetMessage = EmptyMsg()
        self._scoopMsg = ArmCommand()
        self._scoopMsg.phi2 = 0
        self._scoopMsg.phi3 = 0

        # Counters
        self._bushCtr = 0 # Counting number of bushes that has been visited
        self._flagCtr = 0 # Counting number of done flags received
        self._scoopCtr = 0 # Counting number of attempts to scoop
        self._berryCtr = 0 # counting number of detections
        self._berryDetectionCtrThreshold = 1

        # Initialize internal state = -1 (Move-state = 0, Pick state = 1, Turn-state = 2)
        self._internalState = -1

        # To allow for easy looping when moving
        # Thetaberry will update to pi radians when in secound lane of field.
        self._thetaBerry = 0.0
        self._thetaWhenMoving = np.pi/2.0

    def berryDetectionCallback(self, msg):
        # Saves berry detection from message
        if (msg.data == 1) and ((self._flagCtr == 2 or self._flagCtr == 3) and self._internalState == 1):
            self._berryCtr = self._berryCtr + 1

        else:
            self._berryCtr = 0

        if self._berryCtr >= self._berryDetectionCtrThreshold:
            berry = 1
        else:
            berry = 0

        self._berryDetected = berry

    def isDoneCallback(self, msg):
        # What to do when you recieve a flag when target is done.
        if self.targetReady == 0:
            self.targetReady = 1
            self._flagCtr = self._flagCtr + 1
        else:
            print('Got is done at invalid time !')

    def getCurrentState(self):
        # Returns current internal state.
        if self._bushCtr > 2*self._nBush:
            self._internalState = -1

        return self._internalState

    def setState(self, state):
        # Sets current internal state
        if self._bushCtr > 10:
            self._internalState = -1
        
        else:
            self._internalState = state

    def enterState(self, state):
        if state == 0:
            self._moveState()

        elif state == 1:
            self._pickingState()

        elif state == 2:
            self._turnState()

        else:
            self.setState(-1)
            #print('Invalid state, setting to -1')

    def getSP(self):
        # Gets setpoints and returns them
        spx = self._SPVec[self._bushCtr-1][0][0]
        spy = self._SPVec[self._bushCtr-1][0][1]
        print("Setpoints. x: " + str(spx) + " y: " + str(spy))
        return spx, spy

    def pubMsg(self):
        # Publishes message
        self._setPointPub.publish(self._setPointMsg)

    def SCOOP(self, val):
        # Scoops the berry
        self._scoopMsg.phi1 = val
        self._scoopPub.publish(self._scoopMsg)

    def _moveState(self):
        # Increment and move to pos with index bushCtr, enter _pickingState when done
        # Move-state is state 0

        # Increment bush counter
        self._bushCtr = self._bushCtr + 1

        # Get new setpoints
        if self._bushCtr > 2*self._nBush:
            # Done with picking, enters invalid state
            self.setState(-1)
        else:
            setPointX, setPointY = self.getSP()

            # Fill message data
            self._setPointMsg.x = setPointX
            self._setPointMsg.y = setPointY
            self._setPointMsg.theta = self._thetaWhenMoving  + self._thetaBerry
            self._setPointMsg.mode = self._targetMoveMode

            
            # Set target ready to 0 to wait for done signal, change to pick-state
            self.targetReady = 0
            self._internalState = 1

            # Set flagctr to 0 when exiting state
            self._flagCtr = 0

    def _pickingState(self):
        # Turn robot, check for berry, initiate picking if red
        # Picking-state is state 1
        flag = self._flagCtr

        # Initial Turn
        if flag == 1:
            # Turn robot

            # To remedy over rotation issue in lane 2
            if self._bushCtr > 5:
                overRotate = 1
            else:
                overRotate = 0

            self._setPointMsg.theta = self._thetaBerry - np.pi/10*overRotate # will be either 0 or pi
            self._setPointMsg.mode = self._targetTurnMode
            self.targetReady = 0

        # Reset EKF and proceed towards berry
        elif flag == 2:

            # Sleep before check to allow image to stabilize
            time.sleep(self._betterDetectionWaitTimer)

            # Check for berry
            berry = self._berryDetected 

            print("Berry detection status: " + str(berry))

            if berry == 1: 
                print("Strawberry detected! Attacking!")
                # Reset EKF local
                self._resetEkfLocPub.publish(self._resetMessage)

                # Enter berry mode
                self._setPointMsg.mode = self._targetBerryMode
                self.targetReady = 0
            
            else:
                # Skip Scooping step if no berry
                self._flagCtr = 4
                self.targetReady = 1
        
        # Scooping step
        elif flag == 3: 
            # Save berry state to something less tedious to write.
            time.sleep(self._betterDetectionWaitTimer)
            berry = 1.0
            print("Berry detection status: " + str(berry))

            if berry == 1:

                # Scoop up (-1 for up)
                self.SCOOP(-1.0)
                
                # Sleep for 2 secounds
                time.sleep(self._scoopTime)

                # Check for berry
                berry = 0*self._berryDetected
                print("Berry detection status: " + str(berry))
                if (berry == 1) and (self._scoopCtr < 3):
                    self._flagCtr = self._flagCtr - 1 # (Jump back) and ready yourself for scooping
                    #self._setPointMsg.mode = self._targetBerryMode # Keep in berry mode
                    self._scoopCtr = self._scoopCtr + 1
                    self.targetReady = 1 # Wait for done signal
                
                # If no berry, it has been successfully scooped
                else:
                    # Enter return mode
                    print("Berry Harvested first try, setting to return mode")
                    self._setPointMsg.mode = self._targetReturnMode
                    self.targetReady = 0
            
            # if no berry is present return to the line
            else:
                # Enter return mode
                print("No berry, setting to return mode")
                self._setPointMsg.mode = self._targetReturnMode
                self.targetReady = 0
                
        # Post scooping return to path
        elif flag == 4:
            # Turn back to get ready for new move-command
            
            # Scoop down after backing up
            # Scoop down (1 for down)
            self.SCOOP(1.0)

            self._setPointMsg.theta = self._thetaWhenMoving + self._thetaBerry
            self._setPointMsg.mode = self._targetTurnMode
            self.targetReady = 0
            
            # Set flag and scoop counter to 0
            self._flagCtr = 0 
            self._scoopCtr = 0


            # When exiting, check if at end of track, if so, enter turning-state  
            if self._bushCtr == self._nBush:
                print("Entering turning state")
                self._internalState = 2

                
            else:
                self._internalState = 0

        else:
            print("Error, flag value not valid")

        print("# of times Robot has signalled 'Done!':" + str(flag))
        print("# of bushes visited: " + str(self._bushCtr))

    def _turnState(self):
        # Turn-state is state 2
        # Drive to some point in front of aruco 1, turn, drive to point in front of aruco 2, turn -> enter move state
        flag = self._flagCtr
        

        # Move forward towards ArUco marker
        if flag == 1:
            self._setPointMsg.mode = self._targetMoveMode
            self._setPointMsg.x = self._SPVec[self._nBush-1][0][0]
            self._setPointMsg.y = self._SPVec[self._nBush-1][0][1] + 0.22
            print("SPx: " + str(self._setPointMsg.x) + " SPy: " + str(self._setPointMsg.y))
            self.targetReady = 0
        
        # Turn towards other marker
        if flag == 2:
            self._setPointMsg.mode = self._targetTurnMode
            self._setPointMsg.theta = self._thetaBerry
            self.targetReady = 0

        # Drive towards other marker
        if flag == 3:
            self._setPointMsg.mode = self._targetMoveMode
            self._setPointMsg.x =  self._secoundLanePos + 0.10       # Manual adjustment to get it to right spot 
            self._setPointMsg.y = self._SPVec[self._nBush-1][0][1] + 0.22
            print("SPx: " + str(self._setPointMsg.x) + " SPy: " + str(self._setPointMsg.y))
            self.targetReady = 0

        # Update theta offset for secound lane and turn towards it
        if flag == 4:
            self._thetaBerry = np.pi
            self._setPointMsg.mode = self._targetTurnMode
            self._setPointMsg.theta = self._thetaBerry + self._thetaWhenMoving
            self.targetReady = 0


        if flag >= 5:
            # Set flagctr to 0 when exiting state
            self._flagCtr = 0
            self._internalState = 0



if __name__ == '__main__':
    # initialize node
    rospy.init_node('JetbotStateMachine')

    try:

        # Declares rate
        rate = rospy.get_param('~rate', 25.0)

        # Create publisher 
        setPointPub = rospy.Publisher('state/Driver/setpoint', DriverSetpoint, queue_size = 1)
        resetLocPosePublisher = rospy.Publisher('ekf/reset/LocPose', EmptyMsg, queue_size = 1)
        scoopPublisher = rospy.Publisher('armCommand', ArmCommand, queue_size = 1)

        # Creates state machine
        SM = StateMachine(setPointPub, resetLocPosePublisher, scoopPublisher)

        # Subscribers
        isDoneSub = rospy.Subscriber('state/Controller/isDone', EmptyMsg, SM.isDoneCallback)
        strawberryDetectionSub = rospy.Subscriber('state/strawberry/detection', Detection, SM.berryDetectionCallback)
        
        # initial values
        initState = 0 # (Move-state = 0, Pick state = 1, Turn-state = 2)

        # Initialize objects
        SM.setState(initState)
        
        # Keep node alive
        execRate = rospy.Rate(rate)

        print('Jetbot StateMachine: Im alive !')
        
        # Sleep 5 sec before continuing
        time.sleep(1.0)

        while not rospy.is_shutdown():
            
            # Enter state and continue the funny-business if the target is ready
            if SM.targetReady == 1:

                # Get current state
                currentState = SM.getCurrentState()

                # Enter the current state to continue working
                # (Move-state = 0, Pick state = 1, Turn-state = 2)
                SM.enterState(currentState)

                # Publish message to JetbotDriver
                if SM.targetReady == 0:
                    SM.pubMsg()

            # Sleeps the remeining time
            execRate.sleep()

    except rospy.ROSInterruptException:
        pass