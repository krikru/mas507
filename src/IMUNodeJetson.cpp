#include <ros/ros.h>
#include <geometry_msgs/Twist.h>
#include <geometry_msgs/TwistStamped.h>
#include <unistd.h> 		// for usleep()
#include <stdlib.h>
#include "i2c.h"

// ------------------- Addresses: ACCELEROMETER --------------
const short I2C_ACC_ADDR = 0x53; 				// I2C address for accelerometer
const unsigned int ACC_PWR_CTRL = 0x2D; 		// Acc power modes register
const unsigned int ACC_FIFO_CONFIG = 0x38;   	// Fifo configuring acc
const unsigned int ACC_DATA_FORMAT = 0x31; 		// Data Formatting
const unsigned int ACC_RATE_CONFIG = 0x2C;
const unsigned int ACC_INT_CONF = 0x2E; 		// Interrupt configure accelerometer
const unsigned int ACC_INT_IO_MAP = 0x2F; 		// Register for mapping of interrupt to IO


// ------------------- Addresses: GYRO -----------------------
const short I2C_GYR_ADDR = 0x68; 				// I2C address for gyro
const unsigned int GYR_SMPLRT_DIV = 0x15; 		// Range config, default value +/- 2000 deg/s
const unsigned int GYR_DLPF_FS = 0x16; 			// ODR and filter config
const unsigned int GYR_PWR_MGMT = 0x3E;

// ------------------- Other constants --------------------
const float pi = 3.14159265;
const int interruptPin = 7;				  			// Interrupt-pin, unused
const float la_rescale = 256.0/9.81*10.4/9.81;				// Scale at +/- 2g, div by g to get m/s; datasheet value: 256 (+/- 1%)
const float av_rescale = 14.375*180.0/pi;        		// Scale-factor for +/- 2000 deg/s, datasheet says 14.375


// ------------------- Funksjoner --------------------------
void configRegister8(I2CDevice& device, const unsigned int regAddr, const char regVal, int ms); 	// Initialize config-function
void msleep(uint16_t ms);														// Millisec- sleep function
float read_word_2c(I2CDevice& device, unsigned int addr, bool flip); 			// read registers and re-arrange MSB&LSB if flip is true.


int main(int argc, char **argv) {

	printf("IMU-node Created!");

	// Open i2c bus
	int bus;
	if ((bus = i2c_open("/dev/i2c-1")) == -1)
	{
		printf("Unable to open i2c bus");
	}

	// Initialize I2C Devices
	I2CDevice acc;
	i2c_init_device(&acc);
	acc.addr = I2C_ACC_ADDR;
	acc.bus = bus;

	I2CDevice gyr;
	i2c_init_device(&gyr);
	gyr.addr = I2C_GYR_ADDR;
	gyr.bus = bus;

	// User control - setup acc
    configRegister8(acc, ACC_FIFO_CONFIG, 0x08, 2);    	// Disable FIFO -Disabled by default-
	configRegister8(acc, ACC_DATA_FORMAT, 0x00, 2); 	// 10 bit res, full scale 10 bit
	configRegister8(acc, ACC_INT_CONF, 0x80, 2); 		// Enable data ready interrupt (DATA_RDY_EN)
	configRegister8(acc, ACC_INT_IO_MAP, 0x00, 2); 		// Map data ready interrupt to int 
	configRegister8(acc, ACC_RATE_CONFIG, 0x08, 2);		// Set output data rate to 25 Hz


	// User control - setup gyro
	configRegister8(gyr, GYR_SMPLRT_DIV, 39, 2); 			// Set sample-rate divider to 39, gives 25 Hz (39 + 1 in denom. )
    configRegister8(gyr, GYR_DLPF_FS, 0x1B, 2);				// Set gyro internal sample-rate to 1kHz LPF BW: 42Hz
	configRegister8(gyr, GYR_PWR_MGMT, 0x01, 2);			// Make sure gyro has correct power-settings.

    // Wake accelerometer
	configRegister8(acc, ACC_PWR_CTRL, 0x08, 50);


	// Start ROS-node parameters and register name.
	ros::init(argc, argv, "IMU");
	ros::NodeHandle node;
	ros::Publisher pub = node.advertise<geometry_msgs::Twist>("sens/measurment/imu", 0); 	// Publisher buffer length 0
	geometry_msgs::Twist msg;																// initialize message

	// Publish in loop.
	ros::Rate rate = 25;
	while(ros::ok()) {
		// Read accelerometer values
		msg.linear.x = float(read_word_2c(acc, 0x32, false)) / la_rescale;
		msg.linear.y = float(read_word_2c(acc, 0x34, false)) / la_rescale;
		msg.linear.z = float(read_word_2c(acc, 0x36, false)) / la_rescale;

		// Read Gyro values
		msg.angular.x = float(read_word_2c(gyr, 0x1D, true)) / av_rescale;
		msg.angular.y = float(read_word_2c(gyr, 0x1F, true)) / av_rescale;
		msg.angular.z = float(read_word_2c(gyr, 0x21, true)) / av_rescale;

		// Pub & sleep.
		pub.publish(msg);
        rate.sleep();

	}

	i2c_close(bus);

return 0;
}


/*
** Sleep function that takes argument in ms instead of us
** Written to avoid large numbers in code
*/
void msleep(uint16_t ms){
	useconds_t uSec = 1000*ms;
	usleep(uSec);
	
}


/*
** Datasheets describe the minimum number of ms is it recommended to sleep between each consecutive
** write to device registers, a wait has been put at the end of the function to make it easy.
** Recommended values for consecutive writes: 2ms, for soft reset: 50ms
*/
void configRegister8(I2CDevice& device, const unsigned int regAddr, const char regVal, int ms){ 
	
	// Buf contains one byte of data
	char buf[1] = {regVal};
	i2c_write(&device, regAddr, buf, sizeof(buf)); // Write buf to regAddr
	msleep(ms); // sleep ms millisecounds
	
}


/*
** Reads two bytes of data at address 'addr' on device 'device', setting 'flip' to true flips LSB and MSB og the number
** Flip would not be necessary if everyone could just agree on one way to arrange the data in the registers (only slightly triggered)
*/
float read_word_2c(I2CDevice& device, unsigned int addr, bool flip) {
	//cout << showbase << internal << setfill('0'); //For debugging - Print i hex
	// Allocate memory for reading.
	unsigned char buf[2];		// Buffer for read bytes to be saved in
	ssize_t len = sizeof(buf);	// Length of bytes to be read.

	// Read registers and save values
	int readRegs = i2c_read(&device, addr, buf, len);
	int16_t LSB = buf[0];
	int16_t MSB = buf[1];

	// Shift readings to make complete value
	int16_t val = MSB<<8 | LSB;
	int16_t value = val;
	
	if(flip==true)
	{
		value = ((val & 0xFF00)>>8) | ((val & 0x00FF)<<8);  // Switches MSB & LSB as they come in the wrong direction in the register.
	}

	return static_cast<float>(value);
}