import math3d
import numpy as np


def forward(q, a2, a3):

    # DH Transformation
    T01 = math3d.DH(q[0], 0, a1, 0)
    T02 = math3d.DH(q[1], 0 ,a2 ,0)

    # Transform from coordinate {0}->{2}
    T02 = T01@T02

    # Return result
    return T02[0:3,3]


def twoLink(x, y, a1, a2, conf):
    # Conf must be either -1 or 1 for elbow up or down
    D = (x**2 + y**2 - a1**2 - a2**2)

    theta2 = np.arctan2(conf*np.sqrt(1-D**2), D)
    theta1 = np.arctan2(y,x) - np.arctan2(a2*np.sin(theta2), a1 +a2*np.cos(theta2))

    # 
    return (theta1, theta2)

def inverse(p, d1, a2, a3, conf):
    # Create zero array of size 3
    q = np.zeros(3)

    # Assume coordinate z = 0
    # Solve two-link problem
    x = p[0]
    y = p[1]
    q[:2] = twoLink(x, y, a2, a3, conf)




# Declare constants and robot parameters
A = 3
t0 = 0
d1 = 2
a2 = 1.5
a3 = 1.5
tEnd = 2*np.pi
NPoints = 100

# Create time vector and initialize path vector
t = np.linspace(t0, tEnd, NPoints)
p0 = np.array([[A],[0]])
p = np.zeros([2,NPoints])

for N in range(NPoints):
    
    p[:,N] = np.transpose(A*np.array([[np.cos(t[N])],[np.sin(t[N])]]))
    
    print(p[:,N])


# Test FK for path
#######

#######

# Test IK for all angles output from path
#######

#######

# Compare to see that path from IK matches input