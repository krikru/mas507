#!/usr/bin/env python


# Importing librarys
import rospy
import numpy as np
from sensor_msgs.msg import Joy
from mas507.msg import VelocityCommand
from mas507.msg import RobotPose2D
from mas507.msg import DriverSetpoint
from mas507.msg import EmptyMsg
from mas507.msg import Polar


# Class to handle and translate setpoints from global frame to local frame
class SetPointTranslator(object):
    # Object to translate global setpoint into setpoint in robot frame
    # Based on homogenious 2D transformations

    def __init__(self):
        # Position of robot in world frame
        self._pose = np.zeros(3)

        # Setpoint in global frame
        self._setPointGlobal = np.zeros(2)

        # Setpoint in local frame
        self._setpointLocal = np.zeros(2)        
        
    def setRobotPose(self, pose):
        # Function to set robot pose

        self._pose = pose

    def setSetPoint(self, sp):
        # Function to set global setpoint

        self._setPointGlobal = sp

    def getRobotFrameSetPoint(self):
        # Function to get setpoint in local frame

        self._translatePoint()

        return self._setpointLocal

    def _hGlbRob(self):
        # Tranformation from global frame to local frame
        t = self._pose[2]
        s = np.sin(t)
        c = np.cos(t)

        x = self._pose[0]
        y = self._pose[1]

        hMatInv = np.array([[  c, -s,  x],
                            [  s,  c,  y],
                            [  0,  0,  1]])

        hMat = np.linalg.inv(hMatInv)

        return hMat

    def _translatePoint(self):
        # Function to translate global setpoint into setpoint in local frame

        xGlb = self._setPointGlobal[0]
        yGlb = self._setPointGlobal[1]

        homVec = np.array([xGlb, yGlb, 1])

        posLoc = self._hGlbRob().dot(homVec)

        self._setpointLocal[0] = posLoc[0]/posLoc[2]
        self._setpointLocal[1] = posLoc[1]/posLoc[2]

# Class to find angle error from setpoint and pose
class AngleError(object):

    def __init__(self):
        # Setpoint
        self._error = 0.0

        # Robot angle
        self._poseTheta = 0.0

        # Pose setpoint
        self._poseSetPoint = 0.0

    def setRobotPose(self, theta):
        # Function to set the angle of the robot in the world frame
        self._poseTheta = theta

    def setPoseSP(self, theta):
        # Function to set pose setpoint

        # Checks that setpoint is in the range of [0, 2*pi]
        theta = np.remainder(theta, 2*np.pi)

        self._poseSetPoint = theta

    def _compute(self):
        # Function to compute setpoint
        SP = self._poseSetPoint
        PV = self._poseTheta

        e = 0

        # Computes the two possible solutions
        e1 = SP - PV

        if e1 < 0:
            e2 = 2*np.pi + e1
        else:
            e2 = e1 - 2*np.pi

        # Finds the shortest path
        if np.abs(e1) < np.abs(e2):
            e = e1
        else:
            e = e2

        self._error = e

    def getError(self):
        # Computes and returns the theta error

        self._compute()

        return self._error

# Controller to drive to desired cartesian setpoint
class Controller(object):
    # Controller to pursuit a point

    def __init__(self):
        # Maximum velocitys of controller
        self._velocityMax = 0.2
        self._omegaMax = np.pi*3.0/4.0

        # Controller gains, proportion of max velocity
        self._ppVelocityGain = 0.5
        self._ppOmegaGain = 1.5

        self._angOmegaGain = 0.8

        self._berryVelocityGain = 0.5
        self._berryOmegaGain = 0.75

        # Output from controller commands
        self._contCmd = np.zeros(2)

        # Defines offset from target for the controller to be satisfied
        self._distanceOfSatifaction = 0.01
        self._angleOfSatifaction = 10 * np.pi/180.0

        # Berry parameters
        berryMinDist = 0.12
        berryMaxDist = 0.17

        berryDistVec = np.array([berryMinDist, berryMaxDist])
        muxberryMat = np.array([[0.5, 0.5],[-0.5, 0.5]])
        berryDistVecPrime = muxberryMat.dot(berryDistVec)

        self._distToBerry = berryDistVecPrime[0]
        self._berryDistanceOfSatifaction = berryDistVecPrime[1]
        self._angToBerry = 10 * np.pi/180.0

        # Position of robot
        self._poseGlb = np.zeros(3)

        self._poseLoc = np.zeros(2)

        # Position of berry in polar coordinats
        self._berryPos = np.array([0.15, 0.0])

        # Setpoint
        self._poseSp = np.zeros(3)

        # Controller Mode
        self._mode = 0

        # Is done flag, is set when a controller is satisfied with the state
        # is reset when a new setpoint and mode is set
        self._isDone = 0

        # Check to check is pose ekf is online and estimating
        self._poseEkfOnline = 0

        # Flags used in mode 3 and 4 to drive and turn sequentaly, flags are high when bot is done with that step
        self._step1 = 0
        self._step2 = 0

        # Member object
        self._translatorPP = SetPointTranslator()
        self._angleError = AngleError()

    def compute(self):
        # In this function the different control modes are called
        #print('mode, isDone')
        #print(self._mode)
        #print(self._isDone)
        #print('step1, step2')
        #print(self._step1)
        #print(self._step2)


        if int(self._mode) == 0:
            # Do nothing mode
            self._contCmd = np.zeros(2, dtype = float)

        elif (int(self._mode) == 1) and (self._isDone == 0):
            #PurePresuitCOntroller
            contCmd = self._purePresuit()
            self._contCmd = contCmd

        elif (int(self._mode) == 2) and (self._isDone == 0):
            # Angle controller
            contCmd = self._angController()
            self._contCmd = contCmd

        elif (int(self._mode) == 3) and (self._isDone == 0):
            # Drives towards strawberry
            contCmd = self._berryController()
            self._contCmd = contCmd

        elif (int(self._mode) == 4) and (self._isDone == 0):
            # Drive forwards/backwards in local frame
            contCmd = self._driveLocFrame()
            self._contCmd = contCmd

        else:
            # Invalid mode, sets motors to zero
            self._contCmd = np.array([0.0, 0.0])
            #print('Jetbot driver: idel !')

    def _purePresuit(self):

        # Parses self variables to give shorter names
        kv = self._ppVelocityGain
        kw = self._ppOmegaGain

        vMax = self._velocityMax
        wMax = self._omegaMax

        # Gets setpoint in local frame
        self._translatorPP.setRobotPose(self._poseGlb)
        self._translatorPP.setSetPoint(self._poseSp[0:2])
        sp = self._translatorPP.getRobotFrameSetPoint()

        spX = sp[0]
        spY = sp[1]

        # Checks if robot is within radius of setpoint
        if np.abs(spX) > self._distanceOfSatifaction:

            # Sets Velocity commands
            # bang bang control
            velCmd = np.sign(spX)*kv*vMax
            # proportional to y error
            omgCmd = kw*wMax*np.sign(spX)*spY

            # Checks for max velocitys
            if np.abs(velCmd) > vMax:
                velCmd = np.sign(velCmd)*vMax

            if np.abs(omgCmd) > wMax:
                omgCmd = np.sign(omgCmd)*wMax

            # Sets setpoints
            return np.array([velCmd, omgCmd])

        else:
            # Sets setpoints to zero, we are within distance

            # Sets is done flag hight
            self._isDone = 1

            print('JetbotDriver: Done with: Pure Presuit')

            return np.array([0.0, 0.0])

    def _angController(self):

        # Parses self variables to give shorter names
        kw = self._angOmegaGain
        wMax = self._omegaMax

        SP = self._poseSp[2]
        PV = self._poseGlb[2]

        # Gets error
        self._angleError.setPoseSP(SP)
        self._angleError.setRobotPose(PV)

        thetaError = self._angleError.getError()


        if np.abs(thetaError) > self._angleOfSatifaction:

            # Sets Velocity commands, bang bang control
            omgCmd = np.sign(thetaError)*kw*wMax

            # Checks for max velocitys
            if np.abs(omgCmd) > wMax:
                omgCmd = np.sign(omgCmd)*wMax

            # Sets setpoints
            return np.array([0.0, omgCmd])

        else:
            # Sets setpoints to zero, we are within distance

            # Sets is done flag hight
            self._isDone = 1

            print('JetbotDriver: Done with: Angle Control')

            return np.array([0.0, 0.0])

    def _berryController(self):
        # Parses self variables to give shorter names
        kv = self._berryVelocityGain
        kw = self._berryOmegaGain

        vMax = self._velocityMax
        wMax = self._omegaMax

        velCmd = 0.0
        omgCmd = 0.0

        if (self._step1 == 0) or (self._step2 == 0):

            # Step 1 is turn step, step 2 is drive step
            

            # step 1, turn
            if self._step1 == 0:
                # Sets cmds
                velCmd = 0.0

                if np.abs(self._berryPos[1]) > self._angToBerry:
                    omgCmd = np.sign(self._berryPos[1])*kw*wMax
                else:
                    # Sets done with step flag
                    self._step1 = 1

            # step 2, drive
            if self._step1 == 1:
                # Sets cmds
                posError =  self._berryPos[0] - self._distToBerry
                if np.abs(posError) > self._berryDistanceOfSatifaction:
                    velCmd = np.sign(posError)*kv*vMax
                else:
                    # Sets done with step flag
                    self._step2 = 1

                omgCmd = 0.0

            # Checks for max vels
            if np.abs(velCmd) > vMax:
                velCmd = np.sign(velCmd)*vMax

            if np.abs(omgCmd) > wMax:
                omgCmd = np.sign(omgCmd)*wMax

            # Sets setpoints
            return np.array([velCmd, omgCmd])

        else:
            # Sets setpoints to zero, we are within distance

            # Resets drive and turn flags
            self._step1 = 0
            self._step2 = 0

            # Sets is done flag hight
            self._isDone = 1

            print('JetbotDriver: Done with: Berry mode')

            return np.array([0.0, 0.0])

    def _driveLocFrame(self):
        # Parses self variables to give shorter names
        kv = self._berryVelocityGain
        kw = self._berryOmegaGain

        vMax = self._velocityMax
        wMax = self._omegaMax

        velCmd = 0.0
        omgCmd = 0.0

        if (self._step1 == 0) or (self._step2 == 0):

            # Step 1 is drive step, step 2 is turn step

            # step 1, drive
            if self._step1 == 0:
                # Sets cmds
                posError = -self._poseLoc[0]
                if np.abs(posError) > self._distanceOfSatifaction :
                    velCmd = np.sign(posError)*kv*vMax
                else:
                    # Sets done with step flag
                    self._step1 = 1

                omgCmd = 0.0


            # step 2, turn
            if self._step1 == 1:
                # Sets cmds
                velCmd = 0.0

                thetaError = -self._poseLoc[1]
                if np.abs(thetaError) > self._angleOfSatifaction:
                    omgCmd = np.sign(thetaError)*kw*wMax
                else:
                    # Sets done with step flag
                    self._step2 = 1

            # Checks for max vels
            if np.abs(velCmd) > vMax:
                velCmd = np.sign(velCmd)*vMax

            if np.abs(omgCmd) > wMax:
                omgCmd = np.sign(omgCmd)*wMax

            # Sets setpoints
            return np.array([velCmd, omgCmd])

        else:
            # Sets setpoints to zero, we are within distance

            # Resets drive and turn flags
            self._step1 = 0
            self._step2 = 0

            # Sets is done flag hight
            self._isDone = 1

            print('JetbotDriver: Done with: Return mode')

            return np.array([0.0, 0.0])

    def getMotorSpMsg(self):
        # Populates msg to be published
        msg = VelocityCommand()

        msg.xDot = self._contCmd[0]
        msg.thetaDot = self._contCmd[1]

        return msg

    def setSetpoint(self, setPoint):
        # Sets setpoint, also resets is done flag
        
        self._poseSp = setPoint

        self._isDone = 0

    def isDone(self):
        # FUnction to return true if controller is done

        return self._isDone

    def setMode(self, mode):
        # Sets controller mode

        self._mode = int(mode)

    def setGlbPose(self, msg):
        # Function to set pose of robot

        self._poseGlb[0] = msg.x
        self._poseGlb[1] = msg.y
        self._poseGlb[2] = msg.theta

        if self._poseEkfOnline == 0:
            self._poseEkfOnline = 1

    def setLocPose(self, msg):
        # Function to set local pose
        self._poseLoc[0] = msg.x
        self._poseLoc[1] = msg.theta

    def setBerryPos(self, msg):
        # Set berry position
        self._berryPos[0] = msg.radius
        self._berryPos[1] = msg.angle

    def poseEkfOnline(self):
        # Function to return if ekf is online

        return self._poseEkfOnline

# Class for reciving and sending data to state machine
class StateMachineComunicator(object):

    def __init__(self):
        # Setpoint
        self._setpoint = np.zeros(3)

        # Mode
        self._mode = 0

        # Flag to mark if isDone msg is sent, 0 is false, 1 is true, set in send function, reset in set function
        self._isSent = 0

        # Flag to check is msg is new or not
        self._isNew = 0

        # Publisher to send is done to state machine
        self._publisher = rospy.Publisher('state/Controller/isDone', EmptyMsg, queue_size = 1)

        # Subscriber to recive data from
        self._subscriber = rospy.Subscriber('state/Driver/setpoint', DriverSetpoint, self._setModeAndSetpoint)

    def _setModeAndSetpoint(self, msg):
        # Function to set setpoint and mode
        self._setpoint[0] = msg.x
        self._setpoint[1] = msg.y
        self._setpoint[2] = msg.theta

        self._mode = int(msg.mode)

        # Resets is sent flag, so node is ready to send a new
        self._isSent = 0

        self._isNew = 1

    def getSetpoint(self):
        # Function to return setpoint
        return self._setpoint

    def getMode(self):
        # Function to return mode
        return self._mode

    def sendIsDone(self):
        # Function to send is done msg
        
        msg = EmptyMsg()

        if self._isSent == 0:
            # Sets flag so msg is only sent once
            self._isSent = 1
            # Sends msg
            self._publisher.publish(msg)

    def isNew(self):
        # Function to return true if a setpoint is new or not

        if int(self._isNew) == 1:
            # Resets flag
            self._isNew = 0

            # Returns true
            return 1 
        else:
            # Returns false
            return 0


if __name__ == '__main__':
    # initialize node
    rospy.init_node('JetbotDriver')

    try:

        # Declares rate
        rate = rospy.get_param('~rate', 25.0)
        
        # Declears classes
        controller = Controller()
        smCom = StateMachineComunicator()

        # Define subscribers
        poseSubscriber = rospy.Subscriber('ekf/state/pose', RobotPose2D, controller.setGlbPose)
        poseLocSubscriber = rospy.Subscriber('ekf/state/locPose', RobotPose2D, controller.setLocPose)
        
        strbrySybscriber = rospy.Subscriber('sens/strawberry/polar', Polar, controller.setBerryPos)

        # Define publisher
        velocityCmd = rospy.Publisher('controller/velocityCommand', VelocityCommand, queue_size = 1)
        

        # Keep node alive
        execRate = rospy.Rate(rate)

        print('Jetbot Driver: Im alive !')
        while not rospy.is_shutdown():

            
            # Sets mode to controller if data is new
            if int(smCom.isNew()) == 1:
                # Gets setpoint and mode from smCom
                setPoint = smCom.getSetpoint()
                mode = smCom.getMode()

                # Sets mode and SP to controller
                controller.setSetpoint(setPoint)
                controller.setMode(mode)

            # Computes controller output if ekf is online
            if int(controller.poseEkfOnline()) == 1:
                # Computes controller output
                controller.compute()
                
                # Gets controller output
                motorMsg = controller.getMotorSpMsg()

                # Sends motor commands
                velocityCmd.publish(motorMsg)
        
                        

            # Sends is done msg
            if controller.isDone() == 1:
                smCom.sendIsDone()

            # Sleeps the remeining time
            execRate.sleep()

    except rospy.ROSInterruptException:
        pass