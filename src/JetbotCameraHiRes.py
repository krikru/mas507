#!/usr/bin/env python
"""
Node for capturing Jetbot camera
"""

import os
import rospy
import cv2
from cv2 import aruco
import numpy as np
from cv_bridge import CvBridge
from sensor_msgs.msg import Image
from StrawberryDetector import StrawberryDetector
from std_msgs.msg import Int64
from mas507.msg import ArucoMarker, Detection, Polar

class JetbotCamera(object):
    # Set camera capture settings
    FRAME_RATE = 5
    VIDEO_WIDTH = 1440
    VIDEO_HEIGHT = 1080

    def __init__(self):        
        # Initilize GStreamer capture
        self.capture = self.get_gstreamer_capture()

        # Load camera calibration matrix from  Python calibration
        data = np.load('%s/catkin_ws/src/mas507/data/calibHiRes/intrinsicCalibration.npz' % (os.path.expanduser("~")))
        self.calibration_matrix = data['mtx']
        self.distortion_parameters = data['dist']

        # Load color calibration matrix
        data = np.load('%s/catkin_ws/src/mas507/data/calibHiRes/colorCalibration.npz' % (os.path.expanduser("~")))
        self.gain_matrix = data["arr_0"] # Original load without fix
        data.close()

        # Splits up calibration into making a map, and doing the remapping in calibrate
        # member function
        # This is faster than doing cv2.undistort()
        self.map1, self.map2 = cv2.initUndistortRectifyMap(
            self.calibration_matrix,
            self.distortion_parameters,
            np.eye(3),
            self.calibration_matrix,
            (self.VIDEO_WIDTH, self.VIDEO_HEIGHT),
            cv2.CV_32FC1
        )
    
    def read(self):
        # Read raw image
        _, cv_raw = self.capture.read()
    
        # Perform color calibration
        image_float = np.array(cv_raw, float) * self.gain_matrix

        # Clip to keep values between 0 and 255 
        # Using [:] sets the value of object "image" without making a new object
        # i.e. just replaces in-object which does not need a return statement        
        cv_raw[:] = np.clip(image_float, 0, 255)
        cv_raw = cv2.GaussianBlur(cv_raw,(15,15),0)
        return cv_raw

    def calibrate(self, image):
        """
        Perform calibration of input OpenCV image
        """

        # Make copy of input image

        # # Perform color calibration
        # image_float = np.array(image, float) * self.gain_matrix

        # # Clip to keep values between 0 and 255 
        # # Using [:] sets the value of object "image" without making a new object
        # # i.e. just replaces in-object which does not need a return statement        
        # calibrated[:] = np.clip(image_float, 0, 255)

        # Apply camera distortion calibration
        calibrated = image.copy()
        calibrated[:] = cv2.remap(calibrated, self.map1, self.map2, cv2.INTER_LINEAR)
        return calibrated

    
    # Private methods
    def get_gstreamer_capture(self):
        """
        gstreamer_pipeline returns a GStreamer pipeline for capturing from the CSI camera
        Defaults to 1280x720 @ 60fps
        Flip the image by setting the flip_method (most common values: 0 and 2)
        display_width and display_height determine the size of the window on the screen
        """
        gstreamer_pipeline = (
            "nvarguscamerasrc ! "
            "video/x-raw(memory:NVMM), "
            "width=(int)%d, height=(int)%d, "
            "format=(string)NV12, framerate=(fraction)%d/1 ! "
            "nvvidconv flip-method=0 ! "
            "video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! "
            "videoconvert ! "
            "video/x-raw, format=(string)BGR ! appsink drop=true sync=false"
            % (
                self.VIDEO_WIDTH,
                self.VIDEO_HEIGHT,
                self.FRAME_RATE,
                self.VIDEO_WIDTH,
                self.VIDEO_HEIGHT,
            )
        )

        # Using default values in gstreamer_pipeline to capture video
        video_capture = cv2.VideoCapture(gstreamer_pipeline, cv2.CAP_GSTREAMER)
        return video_capture


if __name__ == '__main__':
    # Tries to start image publisher if roscore is properly running
    try:
        # Initialize nodes
        rospy.init_node('jetbotCamera')
        print('JetbotCamera Node: created!')

        # Initilize Jetbot camera instance
        camera = JetbotCamera()

        # CvBridge for converting cv2 to ROS images
        bridge = CvBridge()

        # ROS Image Publishers
        pub_raw = rospy.Publisher('image_raw', Image, queue_size=1)
        pub_calibrated = rospy.Publisher('image_calibrated', Image, queue_size=1)
        pub_markers = rospy.Publisher('image_markers', Image, queue_size=1)
        
        # Strawberry detector
        pub_strawberry_image = rospy.Publisher('strawberry_detection', Image, queue_size=1)
        pub_strawberry_detection = rospy.Publisher('state/strawberry/detection', Detection, queue_size=1)
        pub_strawberry_polar = rospy.Publisher('state/strawberry/polar', Polar, queue_size=1)
        intrinsicCalibration =  np.load('%s/catkin_ws/src/mas507/data/calibHiRes/intrinsicCalibration.npz' % (os.path.expanduser("~")))
        strawberryDetector = StrawberryDetector(pub_strawberry_image, pub_strawberry_polar, pub_strawberry_detection, intrinsicCalibration['mtx'], intrinsicCalibration['dist'])
        intrinsicCalibration.close()

        # Aruco marker publisher and msg declaration
        pub_aruco = rospy.Publisher('sens/measurment/cam', ArucoMarker, queue_size=1)
        arucoMarker = ArucoMarker()

        # Get rate of node
        rate = rospy.get_param('~rate', 2)

        # Start Synchronous ROS node execution
        rate = rospy.Rate(rate)
        
        while not rospy.is_shutdown():
            # Read raw image
            cv_raw = camera.read()

            # Calibrate raw image
            cv_calibrated = camera.calibrate(cv_raw)

            # Detect strawberry
            strawberryDetector.detect(cv_calibrated)

            # Gray scale image for Aruco markers detection
            cv_gray = cv2.cvtColor(cv_calibrated, cv2.COLOR_BGR2GRAY)

            # Marker detection
            aruco_dict = aruco.Dictionary_get(aruco.DICT_4X4_50)
            parameters = aruco.DetectorParameters_create()
            corners, ids, rejectedImgPoints = aruco.detectMarkers(
                cv_gray, aruco_dict, parameters=parameters
            )

            # Print Aruco marker detection onto image
            cv_markers = aruco.drawDetectedMarkers(cv_calibrated.copy(), corners, ids)
            

            # Get aruco marker pose
            marker_size = 0.15 # 15 cm
            rvecs, tvecs, trash = aruco.estimatePoseSingleMarkers(
                corners, marker_size, camera.calibration_matrix, camera.distortion_parameters
            )

            # Defining empty array to store distances to markers in
            dist = []

            if ids is not None:
                for ii in range(0,len(ids)):
                    markers = aruco.drawAxis(
                        cv_markers,
                        camera.calibration_matrix,
                        camera.distortion_parameters,
                        rvecs[ii],
                        tvecs[ii],
                        marker_size/2
                    )

                    # Computing distances to markers
                    normDist = np.linalg.norm(tvecs[ii])
                    dist.append(normDist)

                    # Convert from Rodrigues angles to rotation-matrix R
                    # R, jacobian = cv2.Rodrigues(rvecs[ii])

                    # print('x={}, y={}, z={}'.format(tvecs[ii][0,0], tvecs[ii][0,1], tvecs[ii][0,2]))

                # Finding closest marker, this is the marker we publish coordinats to
                # Command finds the index of the smallest element
                idxClosestElement = dist.index(min(dist))                

                # Populating aruco marker msg
                arucoMarker.tvec = tvecs[idxClosestElement][0]
                arucoMarker.rvec = rvecs[idxClosestElement][0]
                arucoMarker.id = ids[idxClosestElement][0]

                # Publish location and rotation of aruco marker
                pub_aruco.publish(arucoMarker)


            # Publish images to ROS messages
            pub_raw.publish(bridge.cv2_to_imgmsg(cv_raw, "bgr8"))        
            pub_calibrated.publish(bridge.cv2_to_imgmsg(cv_calibrated, "bgr8"))
            pub_markers.publish(bridge.cv2_to_imgmsg(cv_markers, "bgr8"))


            # Sleep remaining time
            rate.sleep()


    except rospy.ROSInterruptException:
        pass
